data Pizza = Prepizza | Capa Ingrediente Pizza deriving Show

data Ingrediente = Salsa | Queso | Jamon | Aceitunas Int deriving Show

pizza = Capa Salsa (Capa Jamon (Capa Jamon (Capa Queso (Capa (Aceitunas 5) (Capa Queso (Capa Salsa (Capa Jamon Prepizza)))))))

pApta = Capa Salsa (Capa Jamon (Capa Jamon (Capa Salsa (Capa (Aceitunas 22) (Capa Jamon (Capa Salsa (Capa Jamon Prepizza)))))))

pizzaSinDesc = Capa Salsa (Capa (Aceitunas 10) (Capa (Aceitunas 15) (Capa Salsa (Capa Jamon (Capa (Aceitunas 5) Prepizza)))))

pizzaSoloQueso = Capa Queso (Capa Queso Prepizza)

pizzaSoloJamon = Capa Jamon (Capa Jamon Prepizza)

aceitunas (Aceitunas n) =  n
aceitunas _             = 0

cantidadAceitunas :: Pizza -> Int
cantidadAceitunas  = pizaProcesada f 0
                        where f i r = aceitunas i + r

-- Ejercicio 1)

-- A)
cantidadCapasQueCumplen :: (Ingrediente -> Bool) -> Pizza -> Int 
cantidadCapasQueCumplen f Prepizza = 0
cantidadCapasQueCumplen f (Capa i p) = if(f i) 
										then 1 + cantidadCapasQueCumplen f p 
										else cantidadCapasQueCumplen f p

-- B)
conCapasTransformadas :: (Ingrediente -> Ingrediente) -> Pizza -> Pizza
conCapasTransformadas f Prepizza = Prepizza
conCapasTransformadas f (Capa i p) = (Capa (f i) (conCapasTransformadas f p))

-- C)
soloLasCapasQue :: (Ingrediente -> Bool) -> Pizza -> Pizza
soloLasCapasQue f Prepizza = Prepizza
soloLasCapasQue f (Capa i p) = if(f i)
								then (Capa i (soloLasCapasQue f p))
								else soloLasCapasQue f p

-- Ejercicio 2)

-- A)
sinLactosa :: Pizza -> Pizza
sinLactosa p = soloLasCapasQue (\i -> case i of Queso -> False; _ -> True) p

-- B)
aptaIntolerantesLactosa :: Pizza -> Bool
aptaIntolerantesLactosa p = (cantidadCapasQueCumplen (\i -> case i of Queso -> True; _ -> False) (soloLasCapasQue (\i -> case i of Queso -> True; _ -> False) p)) == 0

-- C)
cantidadDeQueso :: Pizza -> Int
cantidadDeQueso p = cantidadCapasQueCumplen (\i -> case i of Queso -> True; _ -> False) (soloLasCapasQue (\i -> case i of Queso -> True; _ -> False) p)

-- D)
conElDobleDeAceitunas :: Pizza -> Pizza
conElDobleDeAceitunas p = conCapasTransformadas (\i -> case i of (Aceitunas n) -> (Aceitunas (n*2)); _ -> i) p

-- Ejercicio 3)

pizzaProcesada :: (Ingrediente -> b -> b) -> b -> Pizza -> b
pizzaProcesada f e Prepizza = e
pizzaProcesada f e (Capa i p) = (f i (pizzaProcesada f e p)) 

-- Ejercicio 4)

-- A)
cantidadCapasQueCumplen' :: (Ingrediente -> Bool) -> Pizza -> Int
cantidadCapasQueCumplen' f p = pizzaProcesada (\i -> (\n -> if(f i) then (n+1) else n)) 0 p


-- B)
conCapasTransformadas' :: (Ingrediente -> Ingrediente) -> Pizza -> Pizza
conCapasTransformadas' f p = pizzaProcesada (\i -> (\p -> (Capa (f i) p))) Prepizza p

-- C)
soloLasCapasQue' :: (Ingrediente -> Bool) -> Pizza -> Pizza
soloLasCapasQue' f p = pizzaProcesada (\i -> (\p -> if(f i) then (Capa i p) else p)) Prepizza p

-- D)
sinLactosa' :: Pizza -> Pizza
sinLactosa' p = pizzaProcesada (\i -> (\p -> if((\ing -> case ing of Queso -> False; _ -> True) i) then (Capa i p) else p)) Prepizza p

-- E)
aptaIntolerantesLactosa' :: Pizza -> Bool
aptaIntolerantesLactosa' p = pizzaProcesada (\i -> (\boolean -> ((\ing -> case ing of Queso -> False; _ -> True) i) && boolean)) True p

-- F)
cantidadDeQueso' :: Pizza -> Int
cantidadDeQueso' p = pizzaProcesada (\i -> (\n -> ((\ing -> case ing of Queso -> 1; _ -> 0) i) + n)) 0 p

-- G)
conElDobleDeAceitunas' :: Pizza -> Pizza
conElDobleDeAceitunas' p = pizzaProcesada (\i -> (\p -> case i of (Aceitunas n) -> (Capa (Aceitunas (n*2)) p); _ -> (Capa i p))) Prepizza p

-- Ejercicio 5)

-- A)
cantidadAceitunas :: Pizza -> Int
cantidadAceitunas p = pizzaProcesada (\i -> (\n -> case i of (Aceitunas m) -> m + n; _ -> n)) 0 p

-- B)
capasQueCumplen :: (Ingrediente -> Bool) -> Pizza -> [Ingrediente]
capasQueCumplen f p = pizzaProcesada (\i -> (\xs -> if(f i) then i:xs else xs)) [] p

-- C)
conDescripcionMejorada :: Pizza -> Pizza
conDescripcionMejorada p = pizzaProcesada (\i -> (\p -> juntar i p)) Prepizza p

juntar :: Ingrediente -> Pizza -> Pizza
juntar (Aceitunas n) (Capa (Aceitunas m) ps) = Capa (Aceitunas (n+m)) ps
juntar ing ps = Capa ing ps

-- D)
conCapasDe :: Pizza -> Pizza -> Pizza
conCapasDe p1 p2 = pizzaProcesada (\i -> (\p -> (Capa i p))) p2 p1

-- E)
primerasNCapas :: Int -> Pizza -> Pizza
primerasNCapas n p = pizzaProcesada f z p n
  where f x r 0 = Prepizza
        f x r n' = (Capa x (r (n' - 1)))
        z n'= z (n' - 1)


primerasNCapas' :: Int -> Pizza -> Pizza
primerasNCapas' 0 p = Prepizza
primerasNCapas' n Prepizza = Prepizza
primerasNCapas' n (Capa i p) = (Capa i (primerasNCapas' (n-1) p))

-------------------------------------------------------------------------------------------------

capasQueCumplen' :: (Ingrediente -> Bool) -> Pizza -> [Ingrediente]
capasQueCumplen' f Prepizza = []
capasQueCumplen' f (Capa i p) = if(f i) then  i : (capasQueCumplen f p) else (capasQueCumplen f p)

conCapasDe' :: Pizza -> Pizza -> Pizza
conCapasDe' Prepizza p2 = p2
conCapasDe' (Capa i p) p2 = Capa i (conCapasDe' p p2)

cantidadDe' :: (Ingrediente -> Bool) -> Pizza -> Int
cantidadDe' f Prepizza = 0
cantidadDe' f (Capa i p) = if(f i) then 1 + cantidadDe' f p else cantidadDe' f p

-----------------------------------------------------------------------------------------

-- Ejercicio 7)

-- A)
map' :: (a -> b) -> [a] -> [b]
map' f [] = []
map' f (x:xs) = (f x) : map f xs

-- B)
filter' :: (a -> Bool) -> [a] -> [a]
filter' f [] = []
filter' f (x:xs) = if (f x) then x : filter' f xs else filter' f xs

-- C)
foldr' :: (a -> b -> b) -> b -> [a] -> b
foldr' f z [] = z
foldr' f z (x:xs) = (f x (foldr' f z xs))

-- D)
recr :: b -> (a -> [a] -> b -> b) -> [a] -> b
recr z f [] = z
recr z f (x:xs) = f x xs (recr z f xs)

-- E)
foldrl :: (a -> a -> a) -> [a] -> a
foldrl f [] = undefined -- REVISAR
foldrl f (x:xs) = f x (foldrl f xs)

-- F)
zipWith' :: (a -> b -> c) -> [a] -> [b] -> [c]
zipWith' f [] ys = []
zipWith' f xs [] = []
zipWith' f (x:xs) ys = (f x (head ys)) : (zipWith' f xs (tail ys))

-- G)
scanr' :: (a -> b -> b) -> b -> [a] -> [b]
scanr' f z [] = [z]
scanr' f z (x:xs) = (f x z) : (scanr f z xs)

concat' :: [[a]] -> [a]
concat' [] = []
concat' (xs:xss) = xs ++ concat' xss

suma' :: (Int, Int) -> Int
suma' (x, y) = x + y

suma :: Int -> Int -> Int
suma x y = x + y

sumatoria :: [Int] -> Int
sumatoria xs = foldr' (+) 0 xs

replicate' :: Int -> a -> [a]
replicate' 0 e = []
replicate' n e = e : replicate' (n-1) e

curry :: ((a, b) -> c) -> a -> b -> c 
curry f x y = f (x, y)

uncurry :: (a -> b -> c) -> (a, b) -> c
uncurry f (x, y) = f x y

length' :: [a] -> Int
length' xs = foldr' (\_ n -> 1 + n) 0 xs

map'' :: (a -> b) -> [a] -> [b]
map'' f xs = foldr (\x xs' -> (f x):xs') [] xs

filter :: (a -> Bool) -> [a] -> [a]
filter f xs = foldr (\x xs' -> if(f x) then xs' else x:xs') [] xs

find :: (a -> Bool) -> [a] -> Maybe a
find f xs = foldr (\x xs' -> if(f x) then Just x else xs') Nothing xs

any :: (a -> Bool) -> [a] -> Bool
any f xs = foldr (\x xs' -> (f x) || xs') False xs

all :: (a -> Bool) -> [a] -> Bool
all f xs = foldr (\x xs' -> (f x) && xs') True xs

countBy :: (a -> Bool) -> [a] -> Int
countBy f xs = foldr (\x xs' -> if(f x) then 1 + xs' else xs') 0 xs

partition :: (a -> Bool) -> [a] -> ([a], [a])
partition f xs = foldr (\x (xs', ys) -> if(f x) then (x:xs', ys) else (xs', x:ys)) ([], []) xs

zipWith'' :: (a -> b -> c) -> [a] -> [b] -> [c]
zipWith'' q xs ys = foldr f z xs ys
	where f x r [] = []
	      f x r (y:ys) = q x y : r ys
	      z [] = []
	      z (y:ys) = []

scanr'' :: (a -> b -> b) -> b -> [a] -> [b]
scanr'' f z xs = foldr (\x xs' -> (f x z):xs') [] xs

takeWhile :: (a -> Bool) -> [a] -> [a]
takeWhile f xs = foldr (\x xs' -> if(f x) then x:xs' else []) [] xs

take :: Int -> [a] -> [a]
take n xs = foldr f z xs n
  where f x r 0  = []
        f x r n' = x : r (n' - 1)
        z n' = []

drop :: Int -> [a] -> [a]
drop n xs = foldr f z xs n
  where f x r 0  = x : r 0
        f x r n' = r (n' - 1)
        z n' = []

(!!) :: Int -> [a] -> a
(!!) n xs = foldr f z xs n
  where f x r 0 = x
        f x r n' = r (n' - 1)
        z n' = z (n' - 1)

twice f x = f (f x)

flattern :: [[a]] -> [a]
flattern xss = foldr (++) [] xss 

evalN :: Char -> Int
evalN 'a' = 1
evalN 'b' = 0

esUno :: Int -> Bool
esUno 1 = True
esUno _ = False

data ExpA = Cte Int | Sum ExpA ExpA | Prod ExpA ExpA deriving Show

noventaYNueve = Prod (Prod (Sum (Cte 9) (Cte 0)) (Sum (Cte 4) (Cte 7))) (Cte 1)

foldExpA ::(Int -> a) -> (a -> a -> a) -> (a -> a -> a) -> ExpA -> a
foldExpA fc fs fp (Cte n) = fc n
foldExpA fc fs fp (Sum e1 e2) = fs (foldExpA fc fs fp e1) (foldExpA fc fs fp e2)
foldExpA fc fs fp (Prod e1 e2) = fp (foldExpA fc fs fp e1) (foldExpA fc fs fp e2)

cantidadDeCeros :: ExpA -> Int
cantidadDeCeros = foldExpA (\n -> if(n == 0) then 1 else 0) (+) (+)

noTieneNegativosExplicitosExpA :: ExpA -> Bool
noTieneNegativosExplicitosExpA = foldExpA (\n -> n >= 0) (\e1 e2 -> e1 && e2) (\e1 e2 -> e1 && e2)

simplificarExpA' :: ExpA -> ExpA
simplificarExpA' = foldExpA (\n -> (Cte n)) (\e1 e2 -> case (e1, e2) of ((Cte 0), e2) -> e2; (e1, (Cte 0)) -> e1; _ -> (Sum e1 e2)) (\e1 e2 -> case (e1, e2) of ((Cte 1), e2) -> e2; (e1, (Cte 1)) -> e1; _ -> (Prod e1 e2)) 

evalExpA' :: ExpA -> Int
evalExpA' = foldExpA id (+) (*)

evalExpA :: ExpA -> Int
evalExpA (Cte n) = n
evalExpA (Sum e1 e2) =  evalExpA e1 + evalExpA e2
evalExpA (Prod e1 e2) = evalExpA e1 * evalExpA e2

showExpA :: ExpA -> String
showExpA = foldExpA (\n -> show n) (\e1 e2 -> "("++ e1 ++ " + " ++ e2 ++ ")") (\e1 e2 -> e1 ++ " * " ++ e2)

---------------------------------------------------------------------------------

data EA = Const Int | BOp BinOp EA EA deriving Show
data BinOp = Suma | Mult deriving Show

noventaYNueve' = BOp Mult (BOp Mult (BOp Suma (Const 9) (Const 0)) (BOp Suma (Const 4) (Const 7))) (Const 1)

foldEA :: (Int -> a) -> (BinOp -> a -> a -> a) -> EA -> a
foldEA fc fb (Const n) = fc n
foldEA fc fb (BOp b e1 e2) = fb b (foldEA fc fb e1) (foldEA fc fb e2)

noTieneNegativosExplicitosExpA' :: EA -> Bool
noTieneNegativosExplicitosExpA' = foldEA (\n -> n >= 0 ) (\b e1 e2 -> e1 && e2)

simplificarEA :: EA -> EA
simplificarEA = foldEA (\n -> (Const n)) (\b e1 e2 -> case b of Suma -> simplificarSuma e1 e2; Mult -> simplificarMult e1 e2)

simplificarSuma :: EA -> EA -> EA
simplificarSuma (Const 0) e2 = e2
simplificarSuma e1 (Const 0) = e1
simplificarSuma e1 e2 = (BOp Suma e1 e2)

simplificarMult :: EA -> EA -> EA
simplificarMult (Const 1) e2 = e2
simplificarMult e1 (Const 1) = e1
simplificarMult e1 e2 = (BOp Mult e1 e2)

evalEA' :: EA -> Int
evalEA' = foldEA id (\b e1 e2 -> case b of Suma -> e1 + e2; Mult -> e1 * e2)

evalEA :: EA -> Int
evalEA (Const n) = n
evalEA (BOp Suma e1 e2) = evalEA e1 + evalEA e2
evalEA (BOp Mult e1 e2) = evalEA e1 + evalEA e2

showEA :: EA -> String
showEA = foldEA (\n -> show n) (\b e1 e2 -> case b of Suma -> "("++ e1 ++ " + " ++ e2 ++ ")"; Mult -> e1 ++ " * " ++ e2)

ea2ExpA :: EA -> ExpA
ea2ExpA = foldEA (\n -> (Cte n)) (\b e1 e2 -> case b of Suma -> (Sum e1 e2); Mult -> (Prod e1 e2)) 

data Arbol a b = Hoja b | Rama a (Arbol a b) (Arbol a b) deriving Show

ea2Arbol :: EA -> Arbol BinOp Int
ea2Arbol = foldEA (\n -> (Hoja n)) (\b e1 e2 -> (Rama b e1 e2))

data Tree a = EmptyT | NodeT a (Tree a) (Tree a) deriving Show

arbol :: Tree Int
arbol = NodeT 1 (NodeT 2 (NodeT 4 EmptyT EmptyT) (NodeT 5 EmptyT EmptyT)) (NodeT 3 (NodeT 6 EmptyT EmptyT) (NodeT 7 (NodeT 8 EmptyT EmptyT) EmptyT))

arbol2 :: Tree Char
arbol2 = NodeT 'a' (NodeT 'b' (NodeT 'd' EmptyT EmptyT) (NodeT 'e' EmptyT EmptyT)) (NodeT 'c' (NodeT 'f' EmptyT EmptyT) (NodeT 'g' (NodeT 'h' EmptyT EmptyT) EmptyT))

foldT :: b -> (a -> b -> b -> b) -> Tree a -> b
foldT z f EmptyT = z
foldT z f (NodeT x t1 t2) = f x (foldT z f t1) (foldT z f t2)

mapT :: (a -> b) -> Tree a -> Tree b
mapT f = foldT EmptyT (\x t1 t2 -> (NodeT (f x) t1 t2))

intToChar :: Int -> Char
intToChar n = case n of 1 -> '1'; 2 -> '2'; 3 -> '3'; 4 -> '4'; 5 -> '5'; 6 -> '6'; 7 -> '7'; 8 -> '8' 

sumT :: Tree Int -> Int
sumT = foldT 0 (\x t1 t2 -> x + t1 + t2)

sizeT :: Tree a -> Int
sizeT = foldT 0 (\x t1 t2 -> 1 + t1 + t2)

heightT :: Tree a -> Int
heightT = foldT 0 (\x t1 t2 -> 1 + max t1 t2)

preOrder :: Tree a -> [a]
preOrder = foldT [] (\x t1 t2 -> [x] ++ t1 ++ t2)

inOrder :: Tree a -> [a]
inOrder = foldT [] (\x t1 t2 -> t1 ++ [x] ++ t2)

postOrder :: Tree a -> [a]
postOrder = foldT [] (\x t1 t2 -> t1 ++ t2 ++ [x])

mirrorT :: Tree a -> Tree a
mirrorT = foldT EmptyT (\x t1 t2 -> NodeT x t2 t1)

countByT :: (a -> Bool) -> Tree a -> Int
countByT f = foldT 0 (\x t1 t2 -> if(f x) then 1 + t1 + t2 else t1 + t2)

partitionT :: (a -> Bool) -> Tree a -> ([a], [a])
partitionT f = foldT ([], []) (\x (xs1, ys1) (xs2, ys2) -> if(f x) then (x:xs1 ++ xs2, ys1 ++ ys2) else (xs1 ++ xs2, ys1 ++ (x:ys2)))

zipWithT :: (a -> b -> c) -> Tree a -> Tree b -> Tree c
zipWithT q t1 t2 = foldT z f t1 t2
  where f x r1 r2 EmptyT = EmptyT
        f x r1 r2 (NodeT x' ti td) = (NodeT (q x x') (r1 ti) (r2 td))
        z EmptyT = EmptyT
        z (NodeT x' ti td) = EmptyT -- Aca no es necesario deconstruir el arbol

caminoMasLargo :: Tree a -> [a]
caminoMasLargo = foldT [] (\x t1 t2 -> if(length t1 > length t2) then x : t1 else x : t2)

todosLosCaminos :: Tree a -> [[a]]
todosLosCaminos = foldT [[]] (\x t1 t2 -> map (x:) (t1 ++ t2))

todosLosNiveles :: Tree a -> [[a]]
todosLosNiveles = foldT [] (\x t1 t2 -> [x] : juntar2 t1 t2)

juntar2 :: [[a]] -> [[a]] -> [[a]]
juntar2 [] ys = ys
juntar2 xs [] = xs
juntar2 (x:xs) (y:ys) = (x ++ y) : juntar2 xs ys

nivelN :: Tree a -> Int -> [a]
nivelN t1 n = foldT z f t1 n
   where f x r1 r2 0 = [x] 
         f x r1 r2 n' = r1 (n'- 1) ++ r2 (n' - 1)
         z n' = []

todosLosCaminos' :: Tree a -> [[a]]
todosLosCaminos' EmptyT = []
todosLosCaminos' (NodeT x t1 t2) = agregar x (todosLosCaminos' t1 ++ todosLosCaminos' t2)

-- No pertenece a la practica
agregar :: a -> [[a]] -> [[a]]
agregar a [] = [[a]]
agregar a (x:xs) = (a : x) : agregar a xs



evalME (Cte n) = return n
evalME (Div e1 e2) = evalME e1 >>= \v1 ->
					 evalMe e2 >>= \v2 ->
					 				if v2 = 0 
					 					then fail "no puedo dividir por cero"
					 					else return (v1/v2)