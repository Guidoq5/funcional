data Pizza = Prepizza | Capa Ingrediente Pizza deriving Show
-- Defino el conjunto Pizza mediante las siguientes reglas:
-- 1. Prepizza esta en el Pizza
-- 2. Si ps es una Pizza e i es un Ingrediente entonces Capa i ps esta en el conjunto Pizza

data Ingrediente = Aceitunas Int | Anchoas | Cebolla | Jamon | Queso | Salsa deriving Show

-- ex2
-- f Prepizza = ...
-- f (Capa i ps) = ... f ps ...

ejPizza = (Capa (Aceitunas 3)
			(Capa (Aceitunas 5)
				(Capa Queso 
					(Capa Salsa Prepizza))))

cantidadDeCapas :: Pizza -> Int
cantidadDeCapas Prepizza = 0
cantidadDeCapas (Capa i ps) = 1 + cantidadDeCapas ps

cantidadDeAceitunas :: Pizza -> Int
cantidadDeAceitunas Prepizza = 0
cantidadDeAceitunas (Capa i ps) = aceitunas i + cantidadDeAceitunas ps

aceitunas :: Ingrediente -> Int
aceitunas (Aceitunas n) = n
aceitunas i 			= 0

duplicarAceitunas :: Pizza -> Pizza
duplicarAceitunas Prepizza = Prepizza
duplicarAceitunas (Capa i ps) = Capa (duplicAc i) (duplicarAceitunas ps)

duplicAc (Aceitunas n) = Aceitunas (n*2)
duplicAc i 			   = i

sinLactosa :: Pizza -> Pizza
sinLactosa Prepizza = Prepizza
sinLactosa (Capa i ps) = agregarSiNoEsQueso i (sinLactosa ps)

agregarSiNoEsQueso :: Ingrediente -> Pizza -> Pizza
agregarSiNoEsQueso Queso ps = ps
agregarSiNoEsQueso i 	 ps = Capa i ps

aptaIntolerantesLactosa :: Pizza -> Bool
aptaIntolerantesLactosa Prepizza = True
aptaIntolerantesLactosa (Capa i ps) = noEsQueso i && (aptaIntolerantesLactosa ps)

noEsQueso :: Ingrediente -> Bool
noEsQueso Queso = False
noEsQueso i     = True

conDescripcionMejorada :: Pizza -> Pizza
conDescripcionMejorada Prepizza = Prepizza
conDescripcionMejorada (Capa i ps) =
	juntarSiAceitunasSeguidas i (conDescripcionMejorada ps) 

juntarSiAceitunasSeguidas (Aceitunas n) (Capa (Aceitunas m) ps) =
	Capa (Aceitunas(n+m)) ps
juntarSiAceitunasSeguidas i ps = 
	Capa i ps

-- DEMOSTRACIONES
