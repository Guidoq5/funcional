data Pizza = Prepizza | Capa Ingrediente Pizza

data Ingrediente = Aceitunas Int | Queso | Jamon | Salsa

aceitunas :: Ingrediente -> Int
aceitunas (Aceitunas n) = n
aceitunas _ = 0

cantidadAceitunas :: Pizza -> Int
cantidadAceitunas Prepizza = 0
cantidadAceitunas (Capa ing ps) = aceitunas ing + cantidadAceitunas ps

conDescM :: Pizza -> Pizza
conDescM Prepizza = Prepizza
conDescM (Capa ing ps) = juntar ing (conDescM ps)

juntar :: Ingrediente -> Pizza -> Pizza
juntar (Aceitunas n) (Capa (Aceitunas m) ps) = Capa (Aceitunas (n+m)) ps
juntar ing ps = Capa ing ps


------------------------------------------------------------------------
{-
			¿cantidadAceitunas . conDescM = cantidadAceitunas?
	
Demostracion por principio de extensionalidad:

			¿∀p. cantidadAceitunas (conDescM p) = cantidadAceitunas p?

Sea p una Pizza. Por principio de induccion estructural:

caso base p = Prepizza ¿cantidadAceitunas (conDescM Prepizza) = cantidadAceitunas Prepizza?

caso inductivo p = (Capa i ps) 
T.I) ¿cantidadAceitunas (conDescM (Capa i ps)) = cantidadAceitunas (Capa i ps)?

HI) ¡ cantidadAceitunas (conDescM ps) = cantidadAceitunas ps !


------------------------------------------------
|                caso base (1)                 |
------------------------------------------------
 cantidadAceitunas (conDescM Prepizza)
=                                   (conDescM.1)
 cantidadAceitunas Prepizza                        <-----|
------------------------------------------------         |
|                caso base (2)                 |         |---- lo que queria demostrar
------------------------------------------------         |
 cantidadAceitunas Prepizza                        <-----|


------------------------------------------------
|             caso inductivo (1)               |
------------------------------------------------
 cantidadAceitunas (conDescM (Capa i ps))
=                                      (conDescM.2)
 cantidadAceitunas (juntar i (conDescM ps))
=                                      (lema 1)
 aceitunas i + cantidadAceitunas (conDescM ps)
=                                      (HI)
 aceitunas i + cantidadAceitunas ps                   <-----|
------------------------------------------------            |
|             caso inductivo (2)               |            |
------------------------------------------------            |---- lo que queria demostrar
 cantidadAceitunas (Capa i ps)                              |
=                           (cantidadAceitunas.2)           |
 aceitunas i + cantidadAceitunas ps                   <-----|


-- LEMA 1 --
			¿∀p. cantidadAceitunas (juntar ing p) = aceitunas ing + cantidadAceitunas p?

Sea p una Pizza. Por principio de induccion estructural:

caso base p = Prepizza ¿cantidadAceitunas (juntar ing Prepizza) = aceitunas ing + cantidadAceitunas Prepizza?

HI) ¡ cantidadAceitunas (juntar ing ps) = aceigunas ing + cantidadAceitunas ps !

caso inductivo p = (Capa i ps)
T.I) ¿cantidadAceitunas (juntar ing (Capa i ps)) = aceitunas ing + cantidadAceitunas (Capa i ps)?

------------------------------------------------
|				caso base (1)                  |
------------------------------------------------ 
 cantidadAceitunas (juntar ing Prepizza)
=                                     (juntar.2)
 cantidadAceitunas (Capa ing Prepizza)
=                                     (cantidadAceitunas.2)
 aceitunas ing + cantidadAceitunas Prepizza                  <-----|
                                                                   |
------------------------------------------------                   |---- lo que queria demostrar
|				caso base (2)                  |                   |
------------------------------------------------                   |
 aceitunas ing + cantidadAceitunas Prepizza                  <-----|

------------------------------------------------
|             caso inductivo (1)               |
------------------------------------------------
 cantidadAceitunas (juntar ing (Capa i ps))
=                                         (juntar.2)
 cantidadAceitunas (Capa ing (Capa i ps))
=                                         (cantidadAceitunas.2)
 aceitunas ing + cantidadAceitunas (Capa i ps)                     <-----|
                                                                         |
------------------------------------------------                         |---- lo que queria demostrar
|             caso inductivo (2)               |                         |
------------------------------------------------                         |
 aceitunas ing + cantidadAceitunas (Capa i ps)                     <-----|


      >>>>>>>  Fede, ¿es necesario esta demostracion? ... es para el caso en que los 2 ingredientes sean aceitunas <<<<<<<

----------------------------------------------------------------------------
|             caso inductivo (1) -> ing = (Aceitunas m) i= (Aceitunas n)   |
----------------------------------------------------------------------------
 cantidadAceitunas (juntar ing (Capa i ps))   
 	EN EL CASO DE QUE SE CUMPLA ing = (Aceitunas m) i= (Aceitunas n)
=                                        (juntar.1)
 cantidadAceitunas (Capa (Aceitunas (n+m) ps))
=                                        (cantidadAceitunas.2)
 aceitunas (Aceitunas (n+m)) + cantidadAceitunas ps
=                                        (aceitunas.1)
 n + m + cantidadAceitunas ps
=                                        (aritmetica)
 m + n + cantidadAceitunas ps                                            <-----|
----------------------------------------------------------------------------   |
|             caso inductivo (2) -> ing = (Aceitunas m) i= (Aceitunas n)   |   |
----------------------------------------------------------------------------   |
 aceitunas (Aceitunas m) + cantidadAceitunas (Capa (Aceitunas n) ps)           |
=                                        (cantidadAceitunas.2)                 |---- lo que queria demostrar
 aceitunas (Aceitunas m) + aceitunas (Aceitunas n) + cantidadAceitunas ps      |                  
=                                        (aceitunas.1)                         |
 m + aceitunas (Aceitunas n) + cantidadAceitunas ps                            |
=                                        (aceitunas.1)                         |
 m + n + cantidadAceitunas ps                                            <-----|



-}
------------------------------------------------------------------------