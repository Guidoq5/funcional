data NExp = Var Variable | NCte Int | NBOp NBinOp NExp NExp 
data NBinOp = Add | Sub | Mul | Div | Mod | Pow 
type Variable = String


pasos foldNExp:

Var Variable | NCte Int | NBOp NBinOp NExp NExp 

(Variable -> b) -> NCte Int | NBOp NBinOp NExp NExp
(Variable -> b) -> (Int -> b ) -> NBOp NBinOp NExp NExp
(Variable -> b) -> (Int -> b ) -> (NBinOp -> b -> b -> b) -> NExp -> b


foldNExp :: (Variable -> b) -> (Int -> b ) -> (NBinOp -> b -> b -> b) -> NExp -> b
foldNExp fVar fI fNb (Var var)       = fVar var
foldNExp fVar fI fNb (NCte n)		   = fI n
foldNExp fVar fI fNb (NBOp x r1 r2) = fNb x (foldNExp fVar fI fNb r1) (foldNExp fVar fI fNb r2)


pasos recNExp:

Var Variable | NCte Int | NBOp NBinOp NExp NExp 

(Variable -> b) -> NCte Int | NBOp NBinOp NExp NExp
(Variable -> b) -> (Int -> b ) -> NBOp NBinOp NExp NExp
(Variable -> b) -> (Int -> b ) -> (NBinOp -> b -> b -> b -> NExp -> NExp) -> NExp -> b


recNExp :: (Variable -> b) -> (Int -> b ) -> (NBinOp -> b -> b -> b -> NExp -> NExp) -> NExp -> b
recNExp fVar fI fNb (Var var)       = fVar var
recNExp fVar fI fNb (NCte n)		   = fI n
recNExp fVar fI fNb (NBOp x r1 r2) = fNb x (recNExp fVar fI fNb r1) (recNExp fVar fI fNb r2) r1 r2


simpNexp :: NExp -> NExp
simpNexp = foldNExp Var NCte fNb
		   where 
		   		 fNb Div r1 (NCte 1) = r1
		   		 fNb Div (NCte 1) r2  = r2
		   		 fNb Div r1 r2  = NBOp Div r1 r2
		   		 fNb Pow r1 (NCte 0) = NCte 1
		   		 fNb Pow (NCte 0) r2  = NCte 1
				 fNb Pow r1 (NCte 0) = r1
		   		 fNb Pow (NCte 1) r2  = r2
		   		 fNb Pow r1 r2  = NBOp Pow r1 r2
		   		 fNb Add r1 r2 = NBOp Add r1 r2
		   		 fNb Sub r1 r2 = NBOp Sub r1 r2
		   		 fNb Mul r1 r2 = NBOp Mul r1 r2
		   		 fNb Mod r1 r2 = NBOp Mod r1 r2






-- 5
recNExp :: (Variable -> b) -> (Int -> b ) -> (NBinOp -> b -> b -> b -> NExp -> NExp) -> NExp -> b
recNExp = foldNExp Var NCte fn
			where 
				fn fvar fi fnb (NBOp x r1 r2) =    fnb x (foldNExp fvar fi fNb r1) (foldNExp fvar fi fNb r2)