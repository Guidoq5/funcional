{- Un estudiante tiene su nombre, carrera y sus cursadas.
   Una cursada se compone de una materia y una nota.
   Una materia tiene su nombre y los créditos de la misma
   en la carrera.
   Y las notas pueden ser numéricas o no numéricas.

   Para cada tipo se ofrecen funciones de proyección de
   componentes.
   Finalmente, se ofrecen las funciones necesarias para
   el cálculo de coeficiente usando solamente 2 funciones
   que no llevan más de una línea cada una (para así dar
   el cálculo en un total de 3 líneas usando esquemas ;)).
-}

data Nota = Número Float
          | Aprobado        | Desaprobado 
          | PorEquivalencia | Ausente

esNúmero :: Nota -> Bool
número   :: Nota -> Float 
   -- PRECONDICIÓN: solo funciona para notas numéricas

data Materia = M String Float 
              -- Nombre Créditos
créditos :: Materia -> Créditos

data Carrera = TPI | LIDS
data Cursada = C Materia Nota
materia  :: Cursada -> Materia
nota     :: Cursada -> Nota 

data Estudiante = A String Carrera [Cursada]
                 -- Nombre Carrera Cursadas  
carrera  :: Estudiante -> Carrera

cursadas :: Estudiante -> [Cursada]

tcc :: Carrera -> Float
-- PROPÓSITO: Describe el total de créditos de
--            la carrera dada
tcc TPI  = 266
tcc LIDS = 466

promedio :: [Float] -> Float
promedio ns = sum ns / length ns

promedioDelAlumno :: Estudiante -> Float
-- PROPÓSITO: describe el promedio del estudiante 
--            utilizando solamente las notas numéricas 
--            e incluyendo los aplazos.
promedioDelAlumno = (\e -> promedio (map numero (filter esNúmero (map nota (cursadas e))))) 

totalCréditos :: Estudiante -> Float
-- PROPÓSITO: describe el total de créditos del estudiante
--            en su carrera
totalCréditos = sum . map créditos . map materia . cursadas

coeficiente :: Estudiante -> Float
coeficiente e =
  promedioDelAlumno e / 2 + 5 * totalCréditos e / tcc (carrera e)