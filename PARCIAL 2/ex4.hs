data NExp = Var Variable | NCte Int | NBOp NBinOp NExp NExp 
data NBinOp = Add | Sub | Mul | Div | Mod | Pow 
type Variable = String

foldNExp :: (Variable -> b) -> (Int -> b ) -> (NBinOp -> b -> b -> b) -> NExp -> b
foldNExp fVar fI fNb (Var var)       = fVar var
foldNExp fVar fI fNb (NCte n)		   = fI n
foldNExp fVar fI fNb (NBOp x r1 r2) = fNb x (foldNExp fVar fI fNb r1) (foldNExp fVar fI fNb r2)



evalNExp :: NExp -> Int
evalNExp = foldNExp fVar NCte fNb
			where 
				fNb Add r1 r2 = r1 + r2
				fNb Sub r1 r2 = r1 - r2
				fNb Mul r1 r2 = r1 * r2
				fNb Div r1 r2 = r1 /  r2
				fNb Pow r1 r2 = r1 (^) r2
				fNb Mod r1 r2 = r1 `mod` r2



recNExp :: (Variable -> b) -> (Int -> b ) -> (NBinOp -> b -> b -> b -> NExp -> NExp) -> NExp -> b
recNExp fVar fI fNb (Var var)       = fVar var
recNExp fVar fI fNb (NCte n)		   = fI n
recNExp fVar fI fNb (NBOp x r1 r2) = fNb x (recNExp fVar fI fNb r1) (recNExp fVar fI fNb r2) r1 r2