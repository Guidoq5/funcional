data Tree a = EmptyT | NodeT a (Tree a) (Tree a)

foldT :: b -> (a -> b -> b -> b) -> Tree a -> b
foldT ze f EmptyT = ze
foldT ze f (NodeT x t1 t2) = f x (foldT ze f t1) (foldT ze f t2)

--3.e.iii)

? foldT  EmptyT NodeT  = id ?
= por ppio de ext
? foldT EmptyT NodeT t = id t ?

Prop.: ? foldT EmptyT NodeT t = id t ?

Dem.: sea t:: Tree a, por ppio de induccion sobre la estructura de t...

------------------------------------------------------------------------------

Caso Base) donde t = EmptyT
	? foldT EmptyT NodeT EmptyT = id EmptyT ?

Lado izq)
foldT EmptyT NodeT EmptyT
=	por def foldT.1
EmptyT

Lado der)
id EmptyT
= por def de id
EmptyT

VALE PARA ESTE CASO

------------------------------------------------------------------------------

Caso Inductivo) donde t = NodeT x t1 t2
	? foldT EmptyT NodeT (NodeT x t1 t2) = id (NodeT x t1 t2) ?

H.I.1)	¡foldT EmptyT NodeT  t1 = id t1!
H.I.1)	¡foldT EmptyT NodeT  t2 = id t2!

T.I)	? foldT EmptyT NodeT  (NodeT x t1 t2) = id (NodeT x t1 t2) ?	

Lado izq)
foldT EmptyT NodeT (NodeT x t1 t2)
=	por def de foldT.2
NodeT x (foldT EmptyT NodeT t1) (foldT EmptyT NodeT t2)
= por H.I.1
NodeT x (id t1) (id t2)
= por def de id
NodeT x t1 t2

lado der)
id (NodeT x t1 t2)
= 	por def de id
NodeT x t1 t2

VALE PARA CASO Inductivo