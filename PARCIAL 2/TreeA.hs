# DEMOSTRACION EJEMPLO

data Tree a = EmptyT | NodeT a (Tree a) (Tree a)

foldT :: b -> (a -> b -> b -> b) -> Tree a -> b 
foldT ze fn EmptyT = ze
foldT ze fn (NodeT x t1 t2) = fn x (foldT ze fn t1) (foldT ze fn t2)


mapT :: (a -> b) -> Tree a -> Tree b
mapT f = foldT EmptyT mapNode
		where mapNode x t1 t2 = NodeT (f x) t1 t2


sizeT :: Tree a -> Int
sizeT = foldT 0 sizeNode
		where sizeNode x t1 t2 = 1 + t1 + t2


 Prop.: ? para todo f. sizeT . mapT f = sizeT ?

 Dem.: Por ppio de ext, es lo mismo demostrar

? para todo f, para todo t. sizeT . mapT f t = sizeT t ?
= 	por def de (.)
? para todo f, para todo t. sizeT (mapT f t) = sizeT t ?

 sizeT (mapT f t) = sizeT t
 = equivale a
 foldT 0 sizeNode (foldT EmptyT mapNode f t) = foldT 0 sizeNode t


Caso base) donde t = EmptyT
	? foldT 0 sizeNode (foldT EmptyT mapNode f EmptyT) = foldT 0 sizeNode EmptyT ?


Caso Inductivo) donde t = (NodeT x t1 t2)
	
H.I.1) ! foldT 0 sizeNode (foldT EmptyT mapNode f t1) = foldT 0 sizeNode t1 !
H.I.2) ! foldT 0 sizeNode (foldT EmptyT mapNode f t2) = foldT 0 sizeNode t2 !

T.I) ? foldT 0 sizeNode (foldT EmptyT mapNode f (NodeT x t1 t2)) = foldT 0 sizeNode (NodeT x t1 t2) ?
    
    
Caso base) ? foldT 0 sizeNode (foldT EmptyT mapNode f EmptyT) = foldT 0 sizeNode EmptyT ? 
    
lado izq)
foldT 0 sizeNode (foldT EmptyT mapNode f EmptyT)
= por def de foldT.1
foldT 0 sizeNode EmptyT

VALE PARA CASO BASE

Caso Inductivo)
    ? foldT 0 sizeNode (foldT EmptyT mapNode f (NodeT x t1 t2)) = foldT 0 sizeNode (NodeT x t1 t2) ?
    
lado izq)
                        
foldT 0 sizeNode (foldT EmptyT mapNode f (NodeT x t1 t2))
= por def de foldT.2
foldT 0 sizeNode ((mapNode f) x (foldT EmptyT (mapNode f) t1) (foldT EmptyT (mapNode f) t2) )
= por H.I.1 Y H.I.2
foldT 0 sizeNode ((mapNode f) x (foldT 0 sizeNode t1) (foldT 0 sizeNode t2))
= def de mapNode 
foldT 0 sizeNode (NodeT (f x) (foldT 0 sizeNode t1)  (foldT 0 sizeNode t2) )
-- def foldT.2
sizeNode (f x) (foldT 0 sizeNode t1) (foldT 0 sizeNode t2)
-- por def de sizeNode
1 + (foldT 0 sizeNode t1) + (foldT 0 sizeNode t2)


lado der)
foldT 0 sizeNode (NodeT x t1 t2)
= por def de foldT
sizeNode x (foldT 0 sizeNode t1) (foldT 0 sizeNode t2)
-- por def de sizeNode
1 + (foldT 0 sizeNode t1) + (foldT 0 sizeNode t2)


VALE CASO INDUCTIVO