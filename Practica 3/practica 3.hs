curry :: ((a,b) -> c) -> a -> b -> c
curry f x y = f (x,y)

--curry :: ((a,b) -> c) -> a -> b -> c
--curry = \f -> \a -> \b -> f(a,b)

uncurry :: (a -> b -> c) -> (a,b) -> c  
uncurry f (x,y) = f x y

--uncurry :: (a -> b -> c) -> (a,b) -> c  
--uncurry = \f -> \(x,y) -> f x y

twice f = g
 where g x = f (f x)

flip f = g   
    where g x = h
          h y = (f y)  x

compose :: (a->b) -> (c->a) -> c -> b
compose f g x = f (g x)

### Ex 7
Dada la siguiente definición, indicar cómo podría reescribirse usando compose y id

many :: Int -> (a -> a) -> a -> a 
many 0 f x = x 
many n f x = f (many (n-1) f x)

many' :: Int -> (a -> a) -> a -> a 
many' 0 f = id
many' n f x = f (many (n-1) f x)
many' n f x = compose f (many (n-1) f) x
many' n f x = (f . (many (n-1) f)) x