data QuadTree a = LeafQ a
				| NodeQ (QuadTree a) (QuadTree a) (QuadTree a) (QuadTree a)

data Color = RGB Int Int Int

type Image = QuadTree Color

heightQt :: QuadTree a -> Int
heightQt (LeafQ x) = 1
heightQt (NodeQ q1 q2 q3 q4) = 1 + (heightQt q1)
									   `max`
								   (heightQt q2)
									   `max`
								   (heightQt q3)
									   `max`
								   (heightQt q4)

countLeavesQT :: QuadTree a -> Int
countLeavesQT (LeafQ x) = 1
countLeavesQT (NodeQ q1 q2 q3 q4) = (countLeavesQT q1)
											+
									(countLeavesQT q2)
											+
									(countLeavesQT q3)
											+
									(countLeavesQT q4)	 

sizeQT :: QuadTree a -> Int
sizeQT (LeafQ x) = 1
sizeQT (NodeQ q1 q2 q3 q4) = 1 + (sizeQT q1)
									  +
								 (sizeQT q2)
								 	  +
								 (sizeQT q3)
								 	  +
								 (sizeQT q4)


compress :: QuadTree a -> QuadTree a
compress (LeafQ x) =
compress (NodeQ q1 q2 q3 q4) = 
	let x = elemLeafQT q1
		in if igualQT q1 q2
			&& igualQT q2 q3
			&& igualQT q3 q4
			&& eqElemQT x q1
			&& eqElemQT x q2
			&& eqElemQT x q3
			&& eqElemQT x q4
			then LeafQ x
			else NodeQ  (compress q1)
						(compress q2)
						(compress q3)
						(compress q4)


igualQT :: Eq a => QuadTree a -> QuadTree a -> Bool
igualQT (LeafQ x) (LeafQ y) = x == y
igualQT (NodeQ q1 q2 q3 q4) (NodeQ q5 q6 q7 q8) = 
	   igualQT q1 q5
	&& igualQT q2 q6
	&& igualQT q3 q7
	&& igualQT q4 q8
igualQT _ _ = False


eqElemQT :: Eq a => a -> QuadTree a -> Int
eqElemQT e (LeafQ x) = e == x
eqElemQT e (NodeQ q1 q2 q3 q4) =
	eqElemQT e q1
	&&
	eqElemQT e q2
	&&
	eqElemQT e q3
	&&
	eqElemQT e q4

elemLeafQT (LeafQ x) = x
elemLeafQT (NodeQ q1 q2 q3 q4) =elemLeafQT q1


-- uncompress :: QuadTree a -> QuadTree a

-- render :: Image -> Int -> Image
