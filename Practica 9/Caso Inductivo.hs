    Caso Inductivo
        ¿ heightT (NodeT e t1 t2) = length (ramaMasLarga (NodeT e t1 t2)) ?

-- Lado Izq)
heightT (NodeT e t1 t2)
--            por def heightT
1 + (`max` (heightT t1) (heightT t2))
--            por H.I 1 y 2
1 + (`max` (length (ramaMasLarga t1)) (length (ramaMasLarga t2)) )

-- la def de max tiene dos casos, se demuestran ambos:

CASO 1: (length (ramaMasLarga t1)) >= (length (ramaMasLarga t2))

1 + length (ramaMasLarga t1)

CASO 2: (length (ramaMasLarga t1)) < (length (ramaMasLarga t2))

1 + length (ramaMasLarga t2)



-- Lado Der)

length (ramaMasLarga (NodeT e t1 t2))

--                    por def ramaMasLarga
length (e : listaMasLarga (ramaMasLarga t1) (ramaMasLarga t2))

--                     def listaMasLarga
 length (if (lenght t1) >= (lenght t2) then t1 else t2)
 
-- CASO 1:            si se cumple el caso de (lenght t1) >= (lenght t2)
length (e: ramaMasLarga t1)

-- def length 
1 + length ramaMasLarga t1

-- CASO 2:            si se cumple el caso de (lenght t1) < (lenght t2)
length (e: ramaMasLarga t2)

-- def length 
1 + length ramaMasLarga t2