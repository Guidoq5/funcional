data N = Z | S N

evalN :: N -> Int
evalN Z = 0
evalN (S n) = 1 + evalN n

-- comparar con
-- append :: [a] -> [a] -> [a]
-- append [] ys = ys
-- append (x:xs) ys = x : append xs ys

addN :: N -> N -> N
addN Z     m = m
addN (S n) m = S (addN n m)

-- reduzco la multiplicacion
-- en base a la suma
-- m + 3 * m
-- m + m + 2 * m
-- m + m + m + 1 * m
-- m + m + m + m + 0

prodN :: N -> N -> N
prodN Z     m = Z
prodN (S n) m = addN (prodN n m) m 

-- Posible tarea
-- 2 * (5 + 6)
-- =
-- 2 * 5 + 2 * 6

-- prodN x (addN y z)
-- =
-- addN (prodN x y) (prodN x z)

int2N :: Int -> N
int2N 0 = Z
int2N n = S (int2N (n-1))

n2Int :: N -> Int
n2Int = evalN

-- evalN (addN ​n1​ ​n2​) ​=​ evalN ​n1​ + evalN ​n2
-- int2N . evalN ​=​ id
-- evalN . int2N ​=​ id

data DigBin = O | I
type NBin = [DigBin]

evalNB :: NBin -> Int
evalNB []     = 0
evalNB (d:ds) = 
	addDB d (2 * evalNB ds)

addDB :: DigBin -> Int -> Int
addDB O n = n
addDB I n = 1 + n

normalizarNB :: NBin -> NBin
normalizarNB [] = []
normalizarNB (d:ds) =
	consNorm d (normalizarNB ds)

consNorm :: DigBin -> NBin -> NBin
consNorm O [] = []
consNorm d ds = d : ds

succNB :: NBin -> NBin
succNB [] = [I]
succNB (O:ds) = I : ds
succNB (I:ds) = O : succNB ds

addNB :: NBin -> NBin -> NBin
addNB [] m   = m
addNB n  []  = n
addNB (n:ns) (m:ms) =
	addDBs n m (addNB ns ms)

addDBs :: DigBin -> DigBin -> NBin -> NBin
addDBs O O ds = O : ds
addDBs O I ds = I : ds
addDBs I O ds = I : ds
addDBs I I ds = O : succNB ds

nb2int :: NBin -> Int
nb2int = evalNB

int2nb :: Int -> NBin
int2nb = undefined

nb2n :: NBin -> N
nb2n [] = Z
nb2n (d:ds) =
  addB2N d (prodN (nb2n ds) (S (S Z)))

addB2N :: DigBin -> N -> N
addB2N O n = n
addB2N I n = S n

n2nb :: N -> NBin
n2nb Z     = []
n2nb (S n) = succNB (n2nb n)

-- evalNB (addNB ​n1​ ​n2​) ​=​ evalNB ​n1​ + evalNB ​n2
-- nb2n . n2nb ​=​ id
-- n2nb . nb2n ​=​ id

data ExpA = Cte Int
          | Suma ExpA ExpA
          | Mult ExpA ExpA

evalExpA :: ExpA -> Int
evalExpA = undefined

simplificarExpA :: ExpA -> ExpA
simplificarExpA = undefined

data EA = Const Int | BOp BinOp EA EA
data BinOp = Sum | Prod

evalEA :: EA -> Int
evalEA = undefined

ea2ExpA :: EA -> ExpA
ea2ExpA = undefined

expA2ea :: ExpA -> EA
expA2ea = undefined

type NU = [()]

evalNU :: NU -> Int
evalNU [] = 0
evalNU (u:us) = 1 + evalNU us

succNU nu = () : nu

addNU :: NU -> NU -> NU
addNU []     mu = mu
addNU (n:nu) mu = n : addNU nu mu

prodNU :: NU -> NU -> NU
prodNU []     mu = []
prodNU (n:nu) mu = 
	addNU (prodNU nu mu) mu 

elevadoANU :: NU -> NU -> NU
elevadoANU []     mu = [()]
elevadoANU (n:nu) mu = 
	prodNU (elevadoANU nu mu) mu

-- prodNU x (addNU y z) = 
-- 	addNU (prodNU x y) (prodNU x z)

data Tree a = EmptyT | NodeT a (Tree a) (Tree a)

levelN :: Int -> Tree a -> [a]
levelN 0 EmptyT = []
levelN 0 (NodeT x t1 t2) = [x]
levelN n EmptyT = []
levelN n (NodeT x t1 t2) =
	levelN (n-1) t1 ++ levelN (n-1) t2

listPerLevel :: Tree a -> [[a]]
listPerLevel EmptyT = []
listPerLevel (NodeT x t1 t2) = 
	[x] :
	concatPorNivel
	  (listPerLevel t1)
	  (listPerLevel t2)

concatPorNivel [] ys = ys
concatPorNivel xs [] = xs
concatPorNivel (x:xs) (y:ys) =
	(x++y) : concatPorNivel xs ys

ramaMasLarga :: Tree a -> [a]
ramaMasLarga EmptyT = []
ramaMasLarga (NodeT x t1 t2) =
	x : masLarga (ramaMasLarga t1)
	             (ramaMasLarga t2)

masLarga xs ys =
	if length xs > length ys
	   then xs
	   else ys

heightT EmptyT = 0
heightT (NodeT x t1 t2) =
	1 + max (heightT t1) (heightT t2)