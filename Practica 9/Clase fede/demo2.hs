-- Acomodando un poco la def
-- antes de demostrar
-- y tener en cuenta
-- que me falta todo lo que en realidad
-- va pero quería hacerlo más
-- rápido

(nb2n . n2nb) = id

-- Por principio de extensionalidad
Para todo x :: N
(nb2n . n2nb) x = id x

(nb2n . n2nb) x
= -- def .
nb2n (n2nb x)

id x
= -- def id
x


Para todo x :: N
nb2n (n2nb x) ​=​ x

Sea nat un N cualquiera
Demustro por inducción sobre nb

Caso base
nat = Z

¿ nb2n (n2nb Z) ​=​ Z ?

nb2n (n2nb Z) ​
= -- def nb2n (n2nb Z) ​
nb2n []
= -- def nb2n
Z

Caso inductivo
nat = S n

HI) nb2n (n2nb n) ​=​ n
TI) nb2n (n2nb (S n)) ​=​ S n

nb2n (n2nb (S n))
= -- n2nb
nb2n (succNB (n2nb n))
= -- nb2n-succNB
S (nb2n (n2nb n))

S n
= HI)
S (nb2n (n2nb n))

-- Lema nb2n-succNB
Para nb :: NBin
¿ nb2n (succNB nb) = S (nb2n nb) ?

Caso base
nb = []

¿ nb2n (succNB []) = S (nb2n []) ?

nb2n (succNB [])
= -- def succNB
nb2n (I:[])
= -- def nb2n
addB2N I (prodN (nb2n []) (S (S Z)))
= -- def addB2N
S (prodN (nb2n []) (S (S Z)))
= -- def nb2n
S (prodN Z (S (S Z)))
= -- def 
S Z

S (nb2n [])
= -- def nb2n
S Z

Caso inductivo
nb = (d:ds)

HI) nb2n (succNB ds) = S (nb2n ds)
TI) nb2n (succNB (d:ds)) = S (nb2n (d:ds))

nb2n (succNB (d:ds))

S (nb2n (d:ds))
= -- def nb2n
S (addB2N d (prodN (nb2n ds) (S (S Z))))

Caso d = O

¿ nb2n (succNB (O:ds)) 
  = S (addB2N O (prodN (nb2n ds) (S (S Z)))) ?

nb2n (succNB (O:ds))
= -- def succNB
nb2n (I:ds)
= -- def nb2n
addB2N I (prodN (nb2n ds) (S (S Z)))
=
S (prodN (nb2n ds) (S (S Z))

S (addB2N O (prodN (nb2n ds) (S (S Z))))
= -- def addB2N
S (prodN (nb2n ds) (S (S Z)))

Caso d = I

¿ nb2n (succNB (I:ds))
  = S (addB2N I (prodN (nb2n ds) (S (S Z)))) ?

nb2n (succNB (I:ds))
= -- def succNB
nb2n (O : succNB ds)
= -- def nb2n
addB2N O (prodN (nb2n (succNB ds)) (S (S Z)))
= -- def addB2N
prodN (nb2n (succNB ds)) (S (S Z))
= -- HI)
prodN (S (nb2n ds)) (S (S Z))
= -- def prodN
addN (S (S Z)) (prodN (nb2n ds) (S (S Z))))
= -- def addN
S (addN (S Z) (prodN (nb2n ds) (S (S Z))))
= -- addN
S (S (addN Z (prodN (nb2n ds) (S (S Z)))))
= -- addN
S (S (prodN (nb2n ds) (S (S Z)))))

S (addB2N I (prodN (nb2n ds) (S (S Z))))
= -- def addB2N
S (S (prodN (nb2n ds) (S (S Z)))))

