-- Casi todo lo que tenía
-- que ir, aunque seguro
-- falte algo, y en el parcial
-- obviamente va todo recontra
-- bien explicado

-- Nota: tiene typos

Para todo nb :: NBin
	evalNB (normalizarNB nb) ​=​ evalNB nb

Case nb = []

¿ evalNB (normalizarNB []) = evalNB []

evalNB (normalizarNB [])
=
evalNB []

Case nb = (d:ds)

HI) evalNB (normalizarNB ds) ​=​ evalNB ds
TI) ¿evalNB (normalizarNB (d:ds)) ​
      =​ evalNB (d:ds)?

evalNB (normalizarNB (d:ds))
= -- normalizarNB
evalNB (consNorm d (normalizarNB ds))
= -- Lema evalNB-consNorm
evalNB (d : normalizarNB ds)

evalNB (d:ds)
= -- def evalNB
addDB d (2 * evalNB ds)
= -- HI)
addDB d (2 * evalNB (normalizarNB ds))
= -- def evalNB
evalNB (d : normalizarNB ds)

Lema evalNB-consNorm:
Para todo d :: DigBing, nb :: NBin
¿ evalNB (consNorm d nb) = evalNB (d:nb) ?

Caso
d  = O
nb = []

¿ evalNB (consNorm O []) = evalNB (O:[]) ?

evalNB (consNorm O [])
= -- def consNorm
evalNB []
= -- def evalNB
0

evalNB (O:[])
= -- def evalNB
addDB O (evalNB [])
= -- def evalNB
addDB O 0
= -- def addDB
0

En otro caso

¿ evalNB (consNorm d ds) = evalNB (d:ds) ?

evalNB (consNorm d ds)
= -- def consNorm
evalNB (d:ds)