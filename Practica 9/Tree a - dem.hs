-- i.heightT = length . ramaMasLarga

¿ heightT = length . ramaMasLarga ?
--								= por ppio de ext
¿ heightT t1 = (length . ramaMasLarga) t1 ?
--								= por def (.)
¿ heightT t1 = length (ramaMasLarga t1) ?

Prop: ¿ para todo t1, heightT t1 = length (ramaMasLarga t1)?

Dem: sea t1:: Tree a, por ppio de induccion sobre la estructura de t1

Caso Base, t1= EmptyT
		¿ heightT EmptyT = length (ramaMasLarga EmptyT) ?
	
Caso Inductivo, t= NodeT e t1 t2

H.I 1) 
		 heightT t1 = length (ramaMasLarga t1) !
H.I 2)
		 heightT t2 = length (ramaMasLarga t2) !
T.I) 
		¿ heightT (NodeT e t1 t2) = length (ramaMasLarga (NodeT e t1 t2)) ?


Caso Base)
		¿ heightT EmptyT = length (ramaMasLarga EmptyT) ?

-- Izq)
heightT EmptyT
--						= por def heightT.1
0

-- Der)
length (ramaMasLarga EmptyT)
--						= por def ramaMasLarga.1
length []
--						= por def length
0
				VALE PARA ESTE CASO


///////////////////////////////////////////////////////

Caso Inductivo, t1= NodeT e t1 t2 )
		¿ heightT (NodeT e t1 t2) = length (ramaMasLarga (NodeT e t1 t2)) ?

-- Izq)
heightT (NodeT e t1 t2)
--					=	por def de heightT.2
1 + max (heightT t1) (heightT t2)
--					=	por H.I 1 y H.I 2
1 + max (length (ramaMasLarga t1)) (length (ramaMasLarga t2))

-- La def de max tiene dos casos, planteo:

Caso 1, length (ramaMasLarga t1) >= length (ramaMasLarga t2) )

1 + length (ramaMasLarga t1)
--					por def length
length (x:ramaMasLarga t1)
--						por def del if
length (if(length (ramaMasLarga t1) < length (ramaMasLarga t2))
							        then x : ramaMasLarga t2
									else x : ramaMasLarga t1 )

--													por def de ramaMasLarga
lenght (ramaMasLarga (NodeT x t1 t2))

VALE PARA ESTE CASO


Caso 2, length (ramaMasLarga t1) < length (ramaMasLarga t2) )

1 + length (ramaMasLarga t2)
--					por def length
length (x:ramaMasLarga t2)
--						por def del if dentro de ramaMasLarga
length (if(length (ramaMasLarga t1) < length (ramaMasLarga t2))
							        then x : ramaMasLarga t2
									else x : ramaMasLarga t1 )
--									por def de ramaMasLarga

lenght (ramaMasLarga (NodeT x t1 t2))

LLEGUE A UNA IGUALDAD CON RESPECTO AL LADO DERECHO

VALE PARA ESTE CASO


VALE LA PROPIEDAD