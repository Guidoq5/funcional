apply f x= f x

swap :: (a,b) -> (b,a)
swap (x, y) = (y, x)

uflip f p = f (swap p)

appFork (f, g) x = (f x, g x)

appDist f (x, y) = (f x, f y)

curry f x y = f(x,y)

uncurry f (x,y) = f x y

flip :: (a -> b -> c) -> (b -> a -> c)
flip f x y = f y x

data Shape = Circle Float | Rect Float Float deriving Show
construyeShNormal :: (Float -> Shape) -> Shape
construyeShNormal c = c 1.0

concatenar::[[a]]->[a]
concatenar [] = []
concatenar (xs:xss) =  xs ++ (concatenar xss)

elem'::Eq a=>a->[a]->Bool
elem' x [] = False
elem' x (y:ys) = x == y || (elem' x ys)

count::(a->Bool)->[a]->Int
count f [] = 0
count f (x:xs) = (if( f x ) then 1 else 0 ) + count f xs

esUno x = x==1

subset::Eq a=>[a]->[a]->Bool
subset [] ys = True
subset xs [] = False
subset (x:xs) ys = elem x ys && subset xs ys


-- es la definicion de (++)
mamas::[a]->[a]->[a]
mamas xs [] = xs
mamas [] ys = ys
mamas xs ys = xs ++ ys

reverse'::[a]->[a]
reverse' [] = [] 
reverse' (x:xs) = (reverse' xs) ++ [x]


unzipCon :: [(a, a)] -> ([a], [a])
unzipCon [] = ([], [])
unzipCon xs = unzipData [] [] xs

unzipData :: [a] -> [a] -> [(a,a)] -> ([a], [a]) 
unzipData xs ys [] = (xs, ys)
unzipData xs ys (d:ds) = unzipData (xs ++ [fst (d)] ) (ys ++ [snd (d)] ) ds