data Tree a = EmptyT | NodeT a (Tree a) (Tree a)

arbolDeNumeros = NodeT 1 (NodeT 2 (NodeT 4 EmptyT EmptyT) (NodeT 5 EmptyT EmptyT)) 
						(NodeT 3 (NodeT 6 EmptyT EmptyT) (NodeT 7 EmptyT EmptyT))

sumarT :: Tree Int -> Int
sumarT EmptyT = 0
sumarT (NodeT x t1 t2) = x + sumarT t1 + sumarT t2

sizeT :: Tree a -> Int
sizeT EmptyT = 0
sizeT (NodeT x t1 t2) = 1 + sizeT t1 + sizeT t2

anyT :: (a->Bool) -> Tree a -> Bool
anyT f EmptyT = False
anyT f (NodeT x t1 t2) =  f x || anyT f t1 || anyT f t2

countT :: (a -> Bool) -> Tree a -> Int
countT f EmptyT = 0
countT f (NodeT x t1 t2) = (if (f x) then 1 else 0) + (countT f t1) + (countT f t2)

-- Las hojas del arbol es cuando tiene emptyT y emptyT por debajo
countLeaves :: Tree a -> Int
countLeaves EmptyT = 0
countLeaves (NodeT x EmptyT EmptyT) = 1
countLeaves (NodeT x t1 t2) = countLeaves t1 + countLeaves t2

heightT :: Tree a -> Int
heightT EmptyT = 0
heightT (NodeT _ EmptyT EmptyT) = 1
heightT (NodeT _ t1 t2) = 1 + max (heightT t1) (heightT t2)

ramaMasLarga :: Tree a -> [a]
ramaMasLarga EmptyT = []
ramaMasLarga (NodeT x t1 t2) = if(length (ramaMasLarga t1) < length (ramaMasLarga t2) )
									then x : ramaMasLarga t2
									else x : ramaMasLarga t1

listInOrder :: Tree a -> [a]
listInOrder EmptyT = []
listInOrder (NodeT x ti td) = listInOrder ti ++ [x] ++ listInOrder td

listPerLevel :: Tree a -> [[a]]
listPerLevel EmptyT = []
listPerLevel (NodeT e t1 t2) = [e] : juntar (listPerLevel t1) (listPerLevel t2)

juntar :: [[a]] -> [[a]] -> [[a]]
juntar xss [] = xss 
juntar [] yss = yss
juntar (xs: xss) (ys:yss) = (xs ++ ys) : (juntar xss yss)

mirrorT :: Tree a -> Tree a
mirrorT EmptyT = EmptyT
mirrorT (NodeT e t1 t2) = NodeT e (mirrorT t2) (mirrorT t1)

levelN :: Int -> Tree a -> [a]
levelN i EmptyT = []
levelN i (NodeT e t1 t2) = case i of
							0 -> [e]
							i -> (levelN (i-1) t1) ++ (levelN (i-1) t2)

ramaMasLarga' :: Tree a -> [a]
ramaMasLarga' EmptyT = []
ramaMasLarga' (NodeT e t1 t2) = e : listaMasLarga (ramaMasLarga' t1) (ramaMasLarga' t2)

listaMasLarga :: [a] -> [a] -> [a]
listaMasLarga xs ys = if (length xs) > (length ys) then xs else ys 

todosLosCaminos :: Tree a -> [[a]]
todosLosCaminos EmptyT = [] 
todosLosCaminos (NodeT e t1 t2) = agregarATodos e (todosLosCaminos t1 ++ todosLosCaminos t2)


agregarATodos :: a -> [[a]] -> [[a]]
agregarATodos e [] = [[e]]
agregarATodos e (xs:xss) = (e : xs) : (agregarATodos e xss)