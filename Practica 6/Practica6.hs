
### Ex 1

doble :: Int -> Int
doble x = x + x

compose :: (a->b) -> (c->a) -> c -> b
compose f g x = f (g x)

cuadruple :: Int -> Int
cuadruple x = 4 * x

a. doble = \x -> 2 * x

doble = \x -> 2 * x
-- Por ppio. de extensionalidad <- se agrega un argumento en ambos lados de la igualdad
doble y = (\x -> 2 * x) y
-- def. doble.1 <- Se indica el nro de definición de la funcion que estamos usando
y + y = (\x -> 2 * x) y
-- Beta reducción
y + y = 2 * y
-- Por Aritmética
y + y = y + y


(f . g) x
f (g x)
(f . g h) x
f ((g h) x)

compose doble doble = cuadruple
-- Por ppio. de extensionalidad
compose doble doble x = cuadruple x
-- reduzco por def. cuadruple
compose doble doble x = 4 * x
-- por def. de compose
doble (doble x) = 4 * x
-- por def. de doble
doble (x + x) = 4 * x
-- por def. de doble
(x + x) + (x + x) = 4 * x
-- por def. asociatividad de la suma
x + x + x + x = 4 * x
-- por Aritmética
x + x + x + x = x + x + x + x

### EX2
fst :: (a, b) -> a
fst (x, y) = x

const :: a -> b -> a
const x  = f where f y' = x

curry :: ((a,b) -> c) -> a -> b -> c
curry f x y = f (x,y)



curry fst = const
-- por ppcpio. de extensionalidad agrego x 
curry fst x = const x
-- por ppcpio. de extensionalidad agrego x 
curry fst x y = const x y
-- def. curry
fst (x,y) = const x y
-- def. fst
x = const x y
-- def. const
x = (f where f y' = x) y
-- por beta reducción
x = x
qed

###EX4 a

¿ curry fst = const ?
-- por ppio. de ext. es lo mismo demostrar:
¿ ∀x. curry fst x = const x ?
-- por ppio. de ext. es lo mismo demostrar:
¿ ∀x.∀y. curry fst x y = const x y ?

-- sean x:a, y:b
¿ curry fst x y = const x y ?

curry fst x y
= def. curry, f=fst x=x y=y
fst (x,y)
= def. fst, (x,y) = (x,y)
x
= def. const, x=x y=y
const x y

Ambos lados llegan a lo mismo, por lo tanto la propiedad vale.