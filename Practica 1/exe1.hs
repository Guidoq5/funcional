--1.
-- lambda
exp1 = \x -> 1+1+1+1
exp2 = \x -> 2*2
exp3 = \x -> case x of 2 -> x*x; _ -> 2*2

--Funciones
suma :: Int -> Int -> Int
suma x y = x + y

-- doble1 (doble 1) (doble 1)
-- doble2 (doble 1) * 2
-- doble2 (doble(doble (doble 0,5)))

--cuadruple1 (cuadruple 1)
--cuadruple1 (doble(cuadruple 0,5))

--2.
doble x = x + x
-- doble (doble 2)
-- reduccion : (doble 2) + (doble 2)
--              (2 + 2) + (doble 2)
--              2 + 2 + 2 + 2
--                 4 + 2 + 2
--                  6 + 2
--                    8

--3
cuadruple x = 4*x
--cuadruple 2
--reduccion:  4*2
--             8

-- cuadruple (cuadruple 2)
--reduccion: 4*(cuadruple 2)
--           4*(4*2)
--           4*4*2
--           16*2
--            32

--4
tripe x = 3*x
sumarDos x = x+2
succ x = x + 1

--5
twice f = g
 where g x = f (f x)

{- twice succ 1
 succ (succ 1)
succ (1 + 1)
succ 1 + 1
1 + 1 + 1
2 + 1
  3
-}


--8
{--
triple \x -> x + x + x
sumar2 \x -> x + 1 + 1
sumar2 = (\x -> sucesor(sucesor x))
succ \x -> 0,5 + x + 0,5 
--}

--9
f x = let (y,z) = (x,x) in y es igual que id x = x

f' (x,y) = 
  let z = x + y 
    in g (z,y) 
  where g (a,b) = a - b

-- f' (5,6)
-- x = 5
-- y = 6
-- z = 11
-- g (11,6)
--    g(a,b) = a - b
--    a = 11
--    b = 6
--  11 - 6
-- 5

--BETA REDUCCION
sumar :: Int -> (Int -> Int)
sumar x = (\y -> x +y)

--masUno x = (sumar 1) x
--masUno 5
--        x=5
-- =
-- (sumar 1) 5
--      x =1
-- =
--  (\y -> 1+y) 5
--        y=5
-- =
--1+5
--6

-- masUno' = sumar 1
-- =
-- sumar 1
--    x=1
-- =
-- (\y -> 1+y)





  



