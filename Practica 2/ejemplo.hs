algo :: a
algo = 5 == 5
-- no se podria dar una expresion ya que A es muy generico para definir un tipo, A puede ser muchas cosas
algo :: Int -> a
algo 5 = a
algo :: a -> b