-- 1)

first :: (a,b) -> a
first (x,y) = x

apply :: ( a -> b ) -> ( a -> b )
-- recibe una funcion y devuelve una funcion aplicada a un parametro
--recibe una funcion y devuelve otra funcion que toma un A y devuelve B
--( a -> b ) es F
apply f = g
    where g x = f x


succ :: Int -> Int
succ x = x + 1

--twice toma una funcion, y la aplica 2 veces. con poner el tipo de twice una sola vez alcanza
-- como twie se aplica 2 veces si o si necesita una funcion de a en a
twice :: (a -> a) -> (a -> a)

twice f = g
    where g x = f (f x)

doble :: Int -> Int
doble x = x + x

swap :: (a,b) -> (b,a)
swap (x, y) = (y, x)

{-
-- unflip recibe a f (que es una funcion)
uflip :: ( -> )
-- f recibe un par "P"
uflip :: ((a, b) -> )
-- f retorna algo de tipo c
uflip :: ((a, b) -> c)
-- unflip devuelve
uflip :: ((a, b) -> c) ->
-- unfliip devuelve la fncion f
uflip :: ((a, b) -> c) -> ( -> )
-- uflip devuelve la funcion f con parametro swap de p que de retorna un C
uflip :: ((a, b) -> c) -> ((b, a) -> c)
-}

uflip f = g
    where g p = f (swap p)


-- 2)

--apply first :: (a,b) -> a

--twice twice :: (a -> a) -> (a -> a)

-- twice swap :: (a,a) -> (a,a)


-- 3)
{-
F = 3
a = 7
b = 2
d = 1
g = 6
c = 5
e=4
-}

-- 5

-- g
funcion :: x -> Bool
funcion a = True
-- con lambda
funcion2 = (\x -> True)

### Ex 3 

const :: a -> (b -> b)
const x = g where g y = y

const' :: a -> b -> a
const x y = x

const'' :: a -> b -> a
const'' x = (\y -> x)

appFork :: ((a -> b), (a -> c)) -> a -> (b, c)
appFork (f, g) = h where h x = (f x, g x)

appDist :: (a -> b) ->  (a,a) -> (b,b) 
appDist f = g where g (x,y) = (f x, f y)