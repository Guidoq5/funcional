import Prelude hiding (Left, Right)

data ExpA = Cte Int
          | Sum ExpA ExpA
          | Prod ExpA ExpA

foldExpA :: (Int -> b)
         -> (b -> b -> b)
         -> (b -> b -> b)
         -> ExpA
         -> b
foldExpA fcte fsum fprod (Cte n) = fcte n
foldExpA fcte fsum fprod (Prod e1 e2) =
    fprod (foldExpA fcte fsum fprod e1)
         (foldExpA fcte fsum fprod e2)
foldExpA fcte fsum fprod (Sum e1 e2) =
    fsum (foldExpA fcte fsum fprod e1)
          (foldExpA fcte fsum fprod e2)

evalExpA :: ExpA -> Int
evalExpA = foldExpA id (*) (+)

cantidadDeCeros :: ExpA -> Int
cantidadDeCeros = foldExpA unoSiCero (+) (+)
   where unoSiCero 0 = 1
         unoSiCero n = 0

cantSumasExpA :: ExpA -> Int
cantSumasExpA = foldExpA (const 0) sumaUno (+)
    where sumaUno r1 r2 = 1 + r1 + r2

simplificarExpA' :: ExpA -> ExpA
simplificarExpA' = foldExpA Cte simpSum simpProd
    where simpSum (Cte 0) m       = m
          simpSum n       (Cte 0) = n
          simpSum n       m       = Sum n m

          simpProd (Cte 1) m       = m
          simpProd n       (Cte 1) = n
          simpProd n       m       = Prod n m

data BinOp = Suma | Mult
data EA = Const Int | BOp BinOp EA EA

foldEA ::(Int -> b) ->(BinOp -> b -> b -> b) ->EA -> b
foldEA fcte fbinop (Const n) = fcte n
foldEA fcte fbinop (BOp binop e1 e2) =
    fbinop binop (foldEA fcte fbinop e1)
                 (foldEA fcte fbinop e2)

cantSumasEA :: EA -> Int
cantSumasEA = foldEA (const 0) sumaUnoSiEsSuma
    where sumaUnoSiEsSuma Suma r1 r2 = 1 + r1 + r2
          sumaUnoSiEsSuma Mult r1 r2 = r1 + r2

data Tree a = EmptyT | NodeT a (Tree a) (Tree a)

foldT :: b -> (a -> b -> b -> b) -> Tree a -> b
foldT ze fn EmptyT = ze
foldT ze fn (NodeT x t1 t2) =
    fn x (foldT ze fn t1)
         (foldT ze fn t2)

mapT :: (a -> b) -> Tree a -> Tree b
mapT f = foldT EmptyT (NodeT . f)

sumT :: Tree Int -> Int
sumT = foldT 0 sumNode
    where sumNode x r1 r2 = x + r1 + r2

sizeT :: Tree Int -> Int
sizeT = foldT 0 sizeNode
    where sizeNode x r1 r2 = 1 + r1 + r2

-- Demo ejemplo:
-- para todo ​f​. ​sizeT . mapT ​f​ ​=​ sizeT

-- sizeT (mapT ​f t)​ ​=​ sizeT t

-- equivale a

-- foldT 0 sizeNode (foldT EmptyT (NodeT . f) t)
-- = foldT 0 sizeNode t

-- Caso base t = EmptyT

-- foldT 0 sizeNode (foldT EmptyT (NodeT . f) EmptyT)
-- = -- def foldT.1
-- foldT 0 sizeNode EmptyT

-- Caso inductivo
-- t = NodeT x t1 t2

-- HI 1)
-- foldT 0 sizeNode (foldT EmptyT (NodeT . f) t1)
-- = foldT 0 sizeNode t1

-- HI 2)
-- foldT 0 sizeNode (foldT EmptyT (NodeT . f) t2)
-- = foldT 0 sizeNode t2

-- TI)
-- foldT 0 sizeNode (foldT EmptyT (NodeT . f) (NodeT x t1 t2))
-- = foldT 0 sizeNode (NodeT x t1 t2)

-- foldT 0 sizeNode (NodeT x t1 t2)
-- = -- def foldT.2
-- sizeNode x 
--          (foldT 0 sizeNode t1)
--          (foldT 0 sizeNode t2)
-- = -- def sizeNode
-- 1 + (foldT 0 sizeNode t1) + (foldT 0 sizeNode t2)

-- foldT 0 sizeNode (foldT EmptyT (NodeT . f) (NodeT x t1 t2))
-- = -- def foldT.2
-- foldT 0 sizeNode 
--   ((NodeT . f) x 
--        (foldT EmptyT (NodeT . f) t1) (foldT EmptyT (NodeT . f) t2))
-- = -- def (.)
-- foldT 0 sizeNode 
--   (NodeT (f x) 
--        (foldT EmptyT (NodeT . f) t1) (foldT EmptyT (NodeT . f) t2))
-- = -- def foldT.2
-- sizeNode (f x)
--           (foldT 0 sizeNode (foldT EmptyT (NodeT . f) t1))
--           (foldT 0 sizeNode (foldT EmptyT (NodeT . f) t2)))
-- = -- HI1 y HI2
-- sizeNode (f x)
--           (foldT 0 sizeNode t1)
--           (foldT 0 sizeNode t2)
-- = -- def sizeNode
-- 1 + (foldT 0 sizeNode t1) + (foldT 0 sizeNode t2)

---------------------------------------------------

data Dir = Left | Right | Straight deriving Eq
data Mapa a = Cofre [a]
            | Nada (Mapa a)
            | Bifurcacion [a] (Mapa a) (Mapa a)

foldMapa :: ([a] -> b)
         -> (b -> b)
         -> ([a] -> b -> b -> b)
         -> Mapa a
         -> b
foldMapa fc fn fb (Cofre xs) = fc xs
foldMapa fc fn fb (Nada r) =
    fn (foldMapa fc fn fb r)
foldMapa fc fn fb (Bifurcacion xs r1 r2) =
    fb xs (foldMapa fc fn fb r1)
          (foldMapa fc fn fb r2)

objects :: Mapa a -> [a]
objects = foldMapa id id soloObj
   where soloObj xs r1 r2 = xs

mapM :: (a -> b) -> Mapa a -> Mapa b
mapM f = foldMapa (Cofre . map f)
                  Nada
                  mapfBif
    where mapfBif xs r1 r2 =
              Bifurcacion (map f xs) r1 r2

has :: (a -> Bool) -> Mapa a -> Bool
has p = foldMapa (any p)
                 id
                 anyBif
    where anyBif xs r1 r2 = any p xs || r1 || r2

hasObjectAt :: (a->Bool) -> Mapa a -> [Dir] -> Bool
hasObjectAt p (Cofre xs) []     = any p xs
hasObjectAt p (Cofre xs) (d:ds) = False
hasObjectAt p (Nada m)   []     = False
hasObjectAt p (Nada m)   (d:ds) =
    case d of
        Straight -> hasObjectAt p m ds
        _        -> False
hasObjectAt p (Bifurcacion xs m1 m2) [] =
    any p xs
hasObjectAt p (Bifurcacion xs m1 m2) (d:ds) =
    case d of
        Left  -> hasObjectAt p m1 ds
        Right -> hasObjectAt p m2 ds
        _     -> False

-- hasObjectAt :: (a->Bool) -> Mapa a -> [Dir] -> Bool
-- hasObjectAt p m ds = 
--  (foldMapa fc fn fb m) ds
--  where fc xs []     = any p xs
--        fc xs (d:ds) = False
--        fn r  []     = False
--        fn r  (d:ds) = 
--              case d of
--                   Straight -> r
--                   _        -> False
--           fb xs r1 r2 [] = any p xs
--           fb xs r1 r2 (d:ds) =
--              case d of
--                  Left  -> r1
--                  Right -> r2
--                  _     -> False

data GTree a = GNode a [GTree a]

-- mapGT :: (a -> b) -> GTree a -> GTree b
-- mapGT f (GNode x gts) = GNode (f x) (applyMapGT f gts)

-- applyMapGT :: (a -> b) -> [GTree a] -> [GTree b]
-- applyMapGT f [] = []
-- applyMapGT f (gt:gts) =
--  (mapGT f gt) : (applyMapGT f gts)

foldGT :: (a -> b -> b)
       -> ([b] -> b)
       -> GTree a
       -> b
foldGT fa fb (GNode x gts) =
    fa x (fb (map (foldGT fa fb) gts))

sumGT :: GTree Int -> Int
sumGT (GNode x gts) = x + sum (map sumGT gts)

sumGT' = foldGT (+) sum

mapGT :: (a -> b) -> GTree a -> GTree b
mapGT f (GNode x gts) = GNode (f x) (map (mapGT f) gts)

-- mapGT' :: (a -> b) -> GTree a -> GTree b
-- mapGT' f = foldGT (\x r -> GNode (f x) r)
--                -- (GNode . f)
--                   (\rs -> rs)
--                -- id

-- Main> sumGT' (GNode 1 [GNode 2 [], GNode 3 []])
-- 6

-- si no hiciese map sumGT gts
-- mapSumGT [] = []
-- mapSumGT (x:xs) = sumGT x : mapSumGT xs

-- Opcion 2
-- sumGT :: GTree Int -> Int
-- sumGT (GNode x gts) = x + applySumGT gts)

-- applySumGT [] = 0
-- applySumGT (x:xs) =
--  sumGT x + applySumGT xs

sizeGT :: GTree a -> Int
sizeGT (GNode x gts) = 1 + sum (map sizeGT gts)

sizeGT' = foldGT (\x r -> 1 + r)
--               (const (+1))
                 (\rs -> sum rs)
--               sum

heightGT :: GTree a -> Int
heightGT (GNode x gts) = 1 + maximum (map heightGT gts)

heightGT' = foldGT (\x r -> 1 + r)
                   (\rs  -> maximum rs)

preOrderGT :: GTree a -> [a]
preOrderGT (GNode x gts) = x : concat (map preOrderGT gts)

preOrderGT' = foldGT (\x r -> x : r)
--                   (:)
                     (\rs  -> concat rs)
--                   concat

-- foldGT (:) concat

posOrderGT :: GTree a -> [a]
posOrderGT (GNode x gts) = concat (map posOrderGT gts) ++ [x]

-- foldGT snoc concat

-- snoc x xs = xs ++ [x]

-- snoc x []     = [x]
-- snoc x (y:ys) = y : snoc x xs

-- snoc x = foldr (:) [x]

inOrderGT :: GTree a -> [a]
inOrderGT (GNode x gts) =
    let xs = concat (map inOrderGT gts)
        (left, right) = splitAt (div (length xs) 2) xs
        in left ++ [x] ++ right

nivelNGT :: GTree a -> Int -> [a]
nivelNGT (GNode x gts) 0 = [x]
nivelNGT (GNode x gts) n =
    concat (map (\gt -> nivelNGT gt (n-1)) gts)

nivelNGT' gt n = foldGT fa fb gt n
    where fa x   r 0 = [x]
          fa x   r n = r (n - 1)
          fb rs    n = concat (map ($ n) rs)





-- f xs ys = zipWith fst xs ys

-- zipWith :: ((b -> c, d) -> b -> c)
--         -> [(b -> c, d)]
--         -> [b]
--         -> [c]
-- zipWith (x:xs) (y:ys) = (fst x) y : zipWith xs ys

-- zipWith fst :: [(b -> c, d)] -> [b] -> [c]

