
data EA = Const Int 
        | BOp BinOp EA EA

data BinOp = Sump | Mul

foldEA :: (Int -> b) -> (BinOp -> b -> b -> b) -> EA -> b
foldEA fcte fbinop (Const i) = fcte i
foldEA fcte fbinop (BOp binop ea1 ea2) = fbinop binop 
                                                (foldEA fcte fbinop ea1)
                                                (foldEA fcte fbinop ea2)


-- noTieneNegativosExplicitosEA :: EA -> Bool
-- noTieneNegativosExplicitosEA = foldEA fCte fBinop
--                                where fCte i = (i >= 0)
--                                      fBinop bOp r1 r2 = r1 && r2
                                     
                                     
-- simplificarEA' :: EA -> EA
-- simplificarEA' = foldEA fCte fBinOp
--                  where  fCte i = Const i
--                         fBinOp Sum (Const 0) r2 = r2
--                         fBinOp Sum r1  (Const 0) = r1
--                         fBinOp Sum r1 r2 = BOp Sum r1 r2
--                         fBinOp Mul (Const 0) r2 = Const 0
--                         fBinOp Mul r1 (Const 0 ) = Const 0
--                         fBinOp Mul (Const 1) r2 = r2
--                         fBinOp Mul r1 (Const 1) = r1
--                         fBinOp Mul r1 r2 = BOp Mul r1 r2
                        

-- evalEA' :: EA -> Int
-- evalEa' = foldEA fCte fBinOp
--           where fCte i = i
--                 fBinOp Sum r1 r2 = r1 + r2
--                 fBinOp Mul r1 r2 = r1 * r2


-- showEA :: EA -> String
-- showEA = foldEA fCte fBinOp
--             where fCte i = show i
--                   fBinOp Sum r1 r2 = "(" ++ r1 ++ " + " ++ r2 ++ ")"
--                   fBinOp Mul r1 r2 = "(" ++ r1 ++ " * " ++ r2 ++ ")"



data ExpA = Cte Int
          | Sum ExpA ExpA
          | Prod ExpA ExpA



ea2ExpA' :: EA -> ExpA
ea2ExpA' = foldEA Cte fBinOp 
            where fBinOp Sump r1 r2 = Sum r1 r2
                  fBinOp Mul r1 r2 = Prod r1 r2

data Arbol a b = Hoja b | Rama a (Arbol a b) (Arbol a b) deriving Show
 
ea2Arbol :: EA -> Arbol BinOp Int
ea2Arbol = foldEA Hoja Rama