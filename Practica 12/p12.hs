data ExpA = Cte Int
          | Sum ExpA ExpA
          | Prod ExpA ExpA deriving Show

-- noventaYNueve = Prod (Prod (Sum (Cte 0) (Cte 9)) (Sum (Cte 4) (Cte 7))) (Cte 1)

foldExpA :: (Int->b) -> (b -> b -> b) -> (b -> b -> b) -> ExpA -> b
foldExpA f g h (Cte i) = f i
foldExpA f g h (Sum n1 n2) = g (foldExpA f g h n1) (foldExpA f g h n2)
foldExpA f g h (Prod n1 n2) = h (foldExpA f g h n1) (foldExpA f g h n2)

cantidadDeCeros :: ExpA -> Int
cantidadDeCeros  = foldExpA fCte fSum fProd
                    where 
                         fCte i = if(i == 0) then 1 else 0
                         fSum r1 r2 = r1 + r2
                         fProd r1 r2 = r1 +r2

noTieneNegativosExplicitosExpA :: ExpA -> Bool
noTieneNegativosExplicitosExpA = foldExpA fCte fSum fProd
                                 where fCte i = (i >= 0)
                                       fSum r1 r2 = r1 && r2
                                       fProd r1 r2 = r1 && r2

simplificarExpA' :: ExpA -> ExpA
simplificarExpA' = foldExpA fCte fSum fProd
                    where fCte i = Cte i

                          fSum r1 (Cte 0) = r1 
                          fSum (Cte 0) r2 = r2
                          fSum r1 r2 = (Sum r1 r2)

                          fProd (Cte 1) r2 = r2
                          fProd r1 (Cte 1) = r1
                          fProd (Cte 0) r2 = (Cte 0)
                          fProd r1 (Cte 0) = (Cte 0)
                          fProd r1 r2 = (Prod r1 r2)

evalExpA' :: ExpA -> Int
evalExpA' = foldExpA id (+) (*)

evalExpA2 = foldExpA fCte fSum fProd
                    where fCte i = i
                          fSum r1 r2 = r1 + r2
                          fProd r1 r2 = r1 * r2

-- cantDeSumaCeros :: ExpA -> Int
-- cantDeSumaCeros = foldExpA fCte fSum fProd
--                   where fCte i = 0
--                         fSum (Cte 0) r2 = 1 + r2
--                         fSum r1 (Cte 0) = r1 + 1
--                         fProd r1 r2 = r1 + r2

-- cantDeProdUnos ::ExpA -> Int
-- cantDeProdUnos = foldExpA fCte fSum fProd 
--                  where fCte i = 0
--                        fSum r1 r2 = r1 + r2
--                        fProd (Cte 1) r2 = 1 + r2
--                        fProd r1 (Cte 1) = 1 + r1


-------------------------------------------------------------
-- foldr :: (a->b->b) -> b -> [a] -> b
-- foldr f z [] = z
-- foldr f z (x:xs) = f x (foldr f z xs)

--3)

data Tree a = EmptyT | NodeT a (Tree a) (Tree a) deriving Show

foldT :: b -> (a -> b -> b -> b) -> Tree a -> b
foldT ze f EmptyT = ze
foldT ze f (NodeT x t1 t2) = f x (foldT ze f t1) (foldT ze f t2) -- siempre fijarse en que necesita la funcion y ahi nos vamos a dar cuenta porque "ze" esta primero que f

arbol :: Tree Int
arbol = NodeT 1 (NodeT 2 (NodeT 4 EmptyT EmptyT) (NodeT 5 EmptyT EmptyT)) (NodeT 3 (NodeT 6 EmptyT EmptyT) (NodeT 7 (NodeT 8 EmptyT EmptyT) EmptyT))


-- mapT :: (a -> b) -> Tree a -> Tree b
-- mapT f = foldT EmptyT (\x t1 t2 -> (NodeT (f x) t1 t2))


mapT :: (a -> b) -> Tree a -> Tree b
mapT f = foldT EmptyT g
         where g x t1 t2 = NodeT (f x) t1 t2

sumT :: Tree Int -> Int
sumT = foldT 0 g
       where g x t1 t2 = x + t1 + t2 

sizeT :: Tree a -> Int
sizeT = foldT 0 g
       where g x t1 t2 = 1 + t1 + t2

heightT :: Tree a -> Int
heightT = foldT 0 g
          where g x t1 t2 = 1 + max t1 t2

preOrder :: Tree a -> [a]
preOrder = foldT [] g
            where g x t1 t2 = [x] ++ t1 ++ t2

inOrder :: Tree a -> [a]
inOrder = foldT [] g
            where g x t1 t2 =  t1 ++ [x] ++ t2


caminoMasLargo :: Tree a -> [a]
caminoMasLargo = foldT [] g
                 where g x t1 t2 = if (length t1 > length t2 ) then [x] ++ t1 else [x] ++ t2

todosLosCaminos :: Tree a -> [[a]]
todosLosCaminos = foldT [] g
                  where g x t1 t2 =  agregar (x) (t1 ++ t2)

----------------------------------------------------------
todosLosCaminos' :: Tree a -> [[a]]
todosLosCaminos' EmptyT = []
todosLosCaminos' (NodeT x t1 t2) = agregar x (todosLosCaminos' t1 ++ todosLosCaminos' t2)

-- No pertenece a la practica
agregar :: a -> [[a]] -> [[a]]
agregar a [] = [[a]]
agregar a (x:xs) = (a : x) : agregar a xs
----------------------------------------------------------


### Ejercicio 4

type Record a b = [(a,b)]

select :: (Record a b -> Bool) -> [Record a b] -> [Record a b]
select f rs = filter f rs

[[(a,b),(a,b)], [(a,b),(a,b)]]

 
project :: (a -> Bool) -> [Record a b] -> [Record a b]
project f [] = []
project f (r:rs) =  if (all (f . fst) r)  then r : project f rs 
                                          else project f rs


conjunct :: (a -> Bool) -> (a -> Bool) -> a -> Bool
conjunct  f g x = (f x) && (g x)