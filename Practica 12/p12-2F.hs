import Prelude hiding (product)

type Record a b = [(a, b)]

data Query a b =
	  Table [Record a b]
	| Product (Query a b) (Query a b)
	| Projection (a -> Bool) (Query a b)
	| Selection (Record a b -> Bool) (Query a b)

-- :t Projection fst
-- (Query (Bool, a) b) -> (Query (Bool, a) b)

-- :t Projection id
-- Query Bool b -> Query Bool b

-- :t Projection null
-- -- Query [a] b -> Query [a] b

-- :t Projection (head . head)
-- -- Query [[Bool]] b -> Query [[Bool]] b

-- pertenece :: Eq a => a -> [a] -> Bool
-- pertenece x = any (== x)

product :: [Record a b] -> [Record a b] -> [Record a b]
product = undefined

select :: (Record a b -> Bool) -> [Record a b] -> [Record a b]
select = undefined

project :: (a -> Bool) -> [Record a b] -> [Record a b]
project = undefined

-- Definir esto con monadas
conjunct :: (a -> Bool) -> (a -> Bool) -> (a -> Bool)
conjunct p1 p2 = (\x -> p1 x && p2 x)

foldQ ::
     ([Record a b] -> c)
  -> (c -> c -> c)
  -> ((a -> Bool) -> c -> c)
  -> ((Record a b -> Bool) -> c -> c)
  -> Query a b
  -> c
foldQ ft fpd fpr fs (Table xs) = ft xs
foldQ ft fpd fpr fs (Product q1 q2) =
   fpd
     (foldQ ft fpd fpr fs q1)
     (foldQ ft fpd fpr fs q2)
foldQ ft fpd fpr fs (Projection pr q) =
   fpr pr (foldQ ft fpd fpr fs q)
foldQ ft fpd fpr fs (Selection pr q) =
   fs  pr (foldQ ft fpd fpr fs q)

-- null :: [a] -> Bool

hayAlgunaTablaVacia :: Query a b -> Bool
hayAlgunaTablaVacia = 
	foldQ null (||) (flip const) (flip const)
	-- foldr (\xs -> null xs)
	--       (\r1 r2 -> r1 || r2)
	--       (\p r -> r)
	--       (\p r -> r)

tables :: Query a b -> [[Record a b]]
tables =
	foldQ (:[]) (++) (flip const) (flip const)

execute :: Query a b -> [Record a b]
execute =
	foldQ id product project select

compact :: Query a b -> Query a b
compact =
	foldQ Table Product simpProj simpSel

simpProj p1 (Projection p2 q) = 
	Projection (conjunct p1 p2) q
simpProj p1 q = Projection p1 q

simpSel p1 (Selection p2 q) =
	Selection (conjunct p1 p2) q
simpSel p1 q = Selection p1 q

-- ((.) ((.) (+)) (+)) x y z
-- -- f = ((+) .)
-- -- g = (+)
-- = -- def (.)
-- (.) (+) ((+) x) y z
-- = -- def (.)
-- -- f = (+)
-- -- g = ((+) x)
-- =
-- (+) (((+) x) y) z
-- =
-- (+) (x+y) z
-- =
-- x + y + z

-- wherefobia

data DiaDeSemana =
	  Lunes
	| Martes
	| Miercoles
	| Jueves
	| Viernes
	| Sabado
	| Domingo
	deriving (Enum, Bounded, Show, Read)

todos :: (Enum a, Bounded a) => [a]
todos = [minBound .. maxBound]

data N = Z | S N deriving Show

instance Enum N where
	succ = S
	toEnum 0 = Z
	toEnum n = S (toEnum (n-1))
	fromEnum Z = 0
	fromEnum (S n) = 1 + fromEnum n

-- data ExpA = Cte Int
--           | Suma ExpA ExpA
--           | Prod ExpA ExpA

-- foldExpA​ :: (Int -> b)
--         -> (b -> b -> b)
--         -> (b -> b -> b)
--         -> ExpA
--         -> b
-- foldExpA​ fcte fs fp (Cte n) = fcte n
-- foldExpA​ fcte fs fp (Suma e1 e2) =
-- 	fs
-- 		(foldExpA​ fcte fs fp e1)
-- 		(foldExpA​ fcte fs fp e2)
-- foldExpA​ fcte fs fp (Prod e1 e2) =
-- 	fp
-- 		(foldExpA​ fcte fs fp e1)
-- 		(foldExpA​ fcte fs fp e2)

-- recExpA​ :: (Int -> b)
--         -> (ExpA -> ExpA -> b -> b -> b)
--         -> (ExpA -> ExpA -> b -> b -> b)
--         -> ExpA
--         -> b
-- recExpA​ fcte fs fp (Cte n) = fcte n
-- recExpA​ fcte fs fp (Suma e1 e2) =
-- 	fs
-- 	    e1
-- 	    e2
-- 		(recExpA​ fcte fs fp e1)
-- 		(recExpA​ fcte fs fp e2)
-- recExpA​ fcte fs fp (Prod e1 e2) =
-- 	fp
-- 	    e1
-- 	    e2
-- 		(recExpA​ fcte fs fp e1)
-- 		(recExpA​ fcte fs fp e2)

-- data Tree a =
-- 	  EmptyT
-- 	| NodeT a (Tree a)
-- 	          (Tree a)

-- foldT :: b -> (a -> b -> b -> b)
--            -> Tree a
--            -> b
-- foldT z f EmptyT = z
-- foldT z f (NodeT x t1 t2) =
-- 	f x (foldT z f t1)
-- 	    (foldT z f t2)

-- todosLosCaminos :: Tree a -> [[a]]
-- todosLosCaminos =
-- 	foldT [] g
-- 	where g x r1 r2 = agregar x (r1 ++ r2)
-- 		  agregar x [] = [[x]]
-- 		  agregar x xs = map (x:) xs

-- -- :t foldr undefined undefined
-- -- [a] -> b

-- -- como
-- f = fold

-- -- en términos
-- f = g fold