-- practica basada en el ejemplo de a practica 7

data Pizza = Prepizza | Capa Ingrediente Prepizza

data Ingrediente =
    Aceitunas Int
    | Queso
    | Salsa
    | Jamon

cantidadDeCapas :: Pizza -> Int
cantidadDeCapas Prepizza = 0
cantidadDeCapas (Capa i ps) = 1 + cantidadDeCapas ps

cantAceitunas :: Pizza -> Int
cantAceitunas Prepizza = 0
cantAceitunas (Capa i ps) = aceitunas i + cantAceitunas ps

aceitunas (Aceitunas n) =  n
aceitunas _             = 0

cantidadAceitunas :: Pizza -> Int
cantidadAceitunas  = pizaProcesada f 0
                        where f i r = aceitunas i + r

dupAceitunas :: Pizza -> Int
dupAceitunas Prepizza = Prepizza
dupAceitunas (Capa i ps) = Capa (dupAcc i) (dupAceitunas ps)

dupAcc (Aceitunas n) = Aceitunas (n*2)
dupAcc i             = i

doble x = x * 2

cantAceitunas . dupAceitunas = doble . cantAceitunas

-- para toda p :: Pizza
(cantAceitunas . dupAceitunas) p  = (doble . cantAceitunas) p

-- que equivale a, por def (.)
cantAceitunas (dupAceitunas p)  = doble (cantAceitunas p)

--por def de doble
cantAceitunas (dupAceitunas p)  = cantAceitunas p * 2

-- sea p una Pizza
-- voy a demostrar por induccion estructural en p 

--Caso Base
-- p = Prepizza

-- ? cantAceitunas (dupAceitunas Prepizza) 
--   = cantAceitunas Prepizza * 2 ?


--Izq: 
-- cantAceitunas (dupAceitunas Prepizza ) 
-- = def dupAceitunas
-- cantAceitunas Prepizza
-- = por def de cantAceitunas
-- = 0

--Der:
--cantAceitunas Prepizza * 2
-- = por def de cantAceitunas
-- = 0 * 2
-- = por def aritmetica
-- = 0

---------------------------------------

-- Caso Inductivo

-- p = (Capa i ps)
-- HI)
-- ? cantAceitunas (dupAceitunas ps)
-- = cantAceitunas ps * 2 ?

-- TI)
-- ? cantAceitunas (dupAceitunas (Capa i ps))
-- = cantAceitunas (Capa i ps) * 2

-- lado DER:
-- cantAceitunas (Capa i ps) * 2

-- por def cantAceitunas
-- (aceitunas i + cantAceitunas ps) * 2

-- distribuyo el * 2
-- aceitunas i * 2 + cantAceitunas ps * 2


----- CAMBIO DE LADO

-- lado IZQ:
--cantAceitunas (dupAceitunas (Capa i ps)) 

-- por def dupAceitunas
-- cantAceitunas (Capa (dupAcc i) (dupAceitunas ps))

-- por def de cantAceitunas
-- = aceitunas (dupAcc i) + cantAceitunas (dupAceitunas ps)

-- por HI)
-- aceitunas (dupAcc i) + cantAceitunas ps * 2

-- = POR LEMA
-- aceitunas i * 2 + cantAceitunas ps * 2


-- Entonces llegue a la igualdad


-- Lema  
-- para todo i :: Ingrediente
 -- ? aceitunas i * 2 = aceitunas (dupAcc i) ?

-- VOY A DEMOSTRAR PARA TODOS LOS CASOS

-- CASO i = Aceitunas n
 -- ? aceitunas (Aceitunas n) * 2 = aceitunas (dupAcc (Aceitunas n)) ?

 -- Lado Izq
 -- aceitunas (Aceitunas n) * 2

-- por def Aceitunas
-- n * 2

-- Lado DER:
-- aceitunas (dupAcc (Aceitunas n))

-- por def dupAcc
-- = aceitunas (Aceitunas (n*2) )

-- por def aceitunas
-- n * 2

--PARA LOS OTROS CASOS, casos que no son (Aceitunas n)
-- selcciono un i' que cumple eso,

 -- ? aceitunas i' * 2 = aceitunas (dupAcc i') ?

 --Lado IZQ)
 -- aceitunas i' * 2

 -- por def aceitunas
 -- 0 * 2
 
 -- = por aritmetica
-- = 0

-- Lado DER)
-- aceitunas (dupAcc i')

-- por def de dupAcc
-- aceitunas i'

-- por def de Aceitunas
-- o

-- por LEMA son iguales

