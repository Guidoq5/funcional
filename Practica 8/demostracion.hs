{-
	¿cantidadAceitunas . conDescM = cantidadAceitunas?

Demostracion por principio de extensionalidad:
	¿∀p. cantidadAceitunas (conDescM p) = cantidadAceitunas p?

Sea p una Pizza. Por principio de induccion estructural:

Caso Base p = Prepizza
	¿cantidadAceitunas (conDescM Prepizza) = cantidadAceitunas Prepizza?

Caso Inductivo p = (Capa i ps)
	¿cantidadAceitunas (conDescM (Capa i ps)) = cantidadAceitunas (Capa i ps)?

HI) ¡ cantidadAceitunas (conDescM ps) = cantidadAceitunas ps ! PORQUE USA PS


Caso Base IZQ
----------------------------------------
 cantidadAceitunas (conDescM Prepizza)
= 							Por def (conDescM.1)
 cantidadAceitunas Prepizza            

Caso Base DER
----------------------------------------
cantidadAceitunas Prepizza 

////////////////////////////////////////

Caso Inductivo IZQ
----------------------------------------
 cantidadAceitunas (conDescM (Capa i ps))
=                                    por def  (conDescM.2)
 cantidadAceitunas (juntar i (conDescM ps))
=                                      (lema 1)
 aceitunas i + cantidadAceitunas (conDescM ps)
=                                      (HI)
 aceitunas i + cantidadAceitunas ps         


Caso Inductivo DER
----------------------------------------
 cantidadAceitunas (Capa i ps)                              
=                           por def (cantidadAceitunas.2)           
 aceitunas i + cantidadAceitunas ps   


/////////////////////////////////////////


-- LEMA 1 --

¿∀p. cantidadAceitunas (juntar ing p) = aceitunas ing + cantidadAceitunas p? PORQUE ESTE LEMA

Sea p una Pizza. Por principio de induccion estructural:

caso base p = Prepizza ¿cantidadAceitunas (juntar ing Prepizza) = aceitunas ing + cantidadAceitunas Prepizza?

caso inductivo p = (Capa i ps) ¿cantidadAceitunas (juntar ing (Capa i ps)) = aceitunas ing + cantidadAceitunas (Capa i ps)?

HI) ¡ cantidadAceitunas (juntar ing ps) = aceigunas ing + cantidadAceitunas ps !  PORQUE USA PS
 
 ESTA H I ES UNA NUEVA O ES LA QUE DEFINIMOS PRIMERO?


Caso Base IZQ
----------------------------------------
 cantidadAceitunas (juntar ing Prepizza)
=                                   por def  (juntar.2)
 cantidadAceitunas (Capa ing Prepizza)
=                                   por def (cantidadAceitunas.2)
 aceitunas ing + cantidadAceitunas Prepizza   

Caso Base DER
----------------------------------------
aceitunas ing + cantidadAceitunas Prepizza

////////////////////////////////////////

Caso Inductivo IZQ
----------------------------------------
 cantidadAceitunas (juntar ing (Capa i ps))
=                                         (juntar.2)
 cantidadAceitunas (Capa ing (Capa i ps))
=                                         (cantidadAceitunas.2)
 aceitunas ing + cantidadAceitunas (Capa i ps) 


Caso Inductivo DER
----------------------------------------
 aceitunas ing + cantidadAceitunas (Capa i ps) 

//////////////////////////////////////////

Demostracion en el caso de que los 2 ingredientes sean aceitunas

caso inductivo IZQ -> ing = (Aceitunas m) i= (Aceitunas n)
----------------------------------------
 cantidadAceitunas (juntar ing (Capa i ps))   
 	EN EL CASO DE QUE SE CUMPLA ing = (Aceitunas m) i= (Aceitunas n)
=                                        (juntar.1)
 cantidadAceitunas (Capa (Aceitunas (n+m) ps))
=                                        (cantidadAceitunas.2)
 aceitunas (Aceitunas (n+m)) + cantidadAceitunas ps
=                                        (aceitunas.1)
 n + m + cantidadAceitunas ps
=                                        (aritmetica)
 m + n + cantidadAceitunas ps   


caso inductivo DER -> ing = (Aceitunas m) i= (Aceitunas n)
----------------------------------------
 aceitunas (Aceitunas m) + cantidadAceitunas (Capa (Aceitunas n) ps)           
=                                        (cantidadAceitunas.2)                 
 aceitunas (Aceitunas m) + aceitunas (Aceitunas n) + cantidadAceitunas ps                        
=                                        (aceitunas.1)                         
 m + aceitunas (Aceitunas n) + cantidadAceitunas ps                            
=                                        (aceitunas.1)                         
 m + n + cantidadAceitunas ps     












-}