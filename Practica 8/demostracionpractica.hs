data Pizza = Prepizza | Capa Ingrediente Pizza

data Ingrediente = Aceitunas Int | Queso | Jamon | Salsa

aceitunas :: Ingrediente -> Int
aceitunas (Aceitunas n) = n
aceitunas _ = 0

cantidadAceitunas :: Pizza -> Int
cantidadAceitunas Prepizza = 0
cantidadAceitunas (Capa ing ps) = aceitunas ing + cantidadAceitunas ps

conDescM :: Pizza -> Pizza
conDescM Prepizza = Prepizza
conDescM (Capa ing ps) = juntar ing (conDescM ps)

juntar :: Ingrediente -> Pizza -> Pizza
juntar (Aceitunas n) (Capa (Aceitunas m) ps) = Capa (Aceitunas (n+m)) ps
juntar ing ps = Capa ing ps

-------------------------------------------------------------------------

-- Debo demostrar que cantidadAceitunas conDescM = cantidadAceitunas

Demostracion por principio de extensionalidad
¿∀:p cantidadAceitunas (conDescM p) = cantidadAceitunas p ?

Sea p una pizza, por induccion estrcutural:

--Caso Base p = Prepizza
¿ cantidadAceitunas (conDescM Prepizza) = cantidadAceitunas Prepizza ?

--Caso Inductivo p = (Capa i ps)
-- T.I      -----> ES ESTA LA TEORIA INDUCTIVA ?
¿ cantidadAceitunas (conDescM (Capa i ps)) = cantidadAceitunas (Capa i ps) ?

-- H.I
¡ cantidadAceitunas (conDescM ps) = cantidadAceitunas ps !

-- Caso Base IZQ)
cantidadAceitunas (conDescM Prepizza)
= por def conDescM
cantidadAceitunas Prepizza

-- Caso Base DER)
cantidadAceitunas Prepizza

-- Caso Inductivo IZQ)
cantidadAceitunas (conDescM (Capa i ps))
= por def de conDescM
cantidadAceitunas  (juntar i (conDescM ps))
= por lema 1
aceitunas i + cantidadAceitunas (conDescM ps)
= por H.I)
aceitunas i + cantidadAceitunas ps

-- Caso Inductivo DER)
cantidadAceitunas (Capa i ps)
= por def cantidadAceitunas
aceitunas i + cantidadAceitunas ps

--------------------- LEMA 1 ---------------------------
¿∀p. cantidadAceitunas (juntar ing p) = aceitunas ing + cantidadAceitunas p?

-- Sea p una Pizza, por principio de induccion estructural
-- Caso Base p = Prepizza
¿cantidadAceitunas (juntar ing Prepizza) = aceitunas ing + cantidadAceitunas Prepizza?

-- Caso Inductivo p = (Capa i ps)
¿cantidadAceitunas (juntar ing (Capa i ps)) = aceitunas ing + cantidadAceitunas (Capa i ps)?

-- H.I)
¡ cantidadAceitunas (juntar ing ps) = aceitunas ing + cantidadAceitunas ps !

-- Caso Base IZQ)
cantidadAceitunas (juntar ing Prepizza)
= por def juntar
cantidadAceitunas (Capa ing Prepizza)
= por def cantidadAceitunas
aceitunas ing + cantidadAceitunas Prepizza

-- Caso Base DER)
aceitunas ing + cantidadAceitunas Prepizza


-- Caso Inductivo IZQ)
cantidadAceitunas (juntar ing (Capa i ps))
= por def juntar
cantidadAceitunas (Capa ing (Capa i ps))
= por def cantidadAceitunas
aceitunas ing + cantidadAceitunas (Capa i ps)

-- Caso Inductivo DER)
aceitunas ing + cantidadAceitunas (Capa i ps)


-- Caso en el que 2 ingredientes son aceitunas

-- Caso Inductivo IZQ) => ing=(Aceituna m) && i=(Aceituna n)
cantidadAceitunas (juntar (Aceituna m) (Capa (Aceituna n) ps))
= por def juntar
cantidadAceitunas ( Capa (Aceitunas (n+m)) ps )
= por def de cantidadAceitunas
aceitunas (Aceitunas (n+m)) + cantidadAceitunas ps
= por def aceitunas
n + m + cantidadAceitunas ps
= por def aritmetica
m + n + cantidadAceitunas ps


-- Caso Inductivo DER) => ing=(Aceituna m) && i=(Aceituna n)
aceitunas (Aceituna m) + cantidadAceitunas (Capa (Aceituna n) ps)
= por def cantidadAceitunas
aceitunas (Aceituna m) + aceitunas (Aceituna n) + cantidadAceitunas ps
= por def aceitunas
m + aceitunas (Aceituna n) + cantidadAceitunas ps
= por def aceitunas
m + n + cantidadAceitunas ps
