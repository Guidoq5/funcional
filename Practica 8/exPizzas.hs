sinLactosa Prepizza = Prepizza
sinLactosa (Capa i ps) = 
    quitarQueso i (sinLactosa ps)

aptaIntLac Prepizza = True
aptaIntLac (Capa i ps) = noEsQueso i && aptaIntLac ps

noEsQueso Queso = False
noEsQueso _ = True

esQueso Queso = True
esQueso _ = False

quitarQueso Queso ps = ps
quitarQueso i     ps = Capa i ps

-- tengo que demostrar:  

--aptaIntLac . sinLactosa = const True

--- Por Ppi Ext, para todo P :: Pizza
--(aptaIntLac . sinLactosa) p = const True p

--- Por def (.)
--aptaIntLac(sinLactosa p) = const True p

--- Por def de const
--aptaIntLac(sinLactosa p) = True


---------- ¿ Aca debo poner CASO BASE ?


--- Elijo una pizza P cualquiera, demuestro por induccion estructural sobre p
--- P = Prepizza

--- ¿  aptaIntLac(sinLactosa Prepizza) = True  ?

---- Lado Izq)

--- Por def de sinLactosa
-- aptaIntLac Prepizza

--- Por def de aptaIntLac
-- True

---- Lado Der)
-- True

---------- CASO INDUCTIVO -- donde P = Capa i ps

--- H.I)
--aptaIntLac(sinLactosa p  = True

--- T.I)
-- ¿  aptaIntLac(sinLactosa (Capa i ps) ) = True  ?

---- Lado Izq)

-- aptIntLac (sinLactosa (Capa i ps) )

--- Por def sinLactosa
-- aptaIntLac (quitarQueso i (sinLactosa ps) )


---- Lado Der)

-- True  






















