data Gusto = Chocolate | DulceDeLeche | Frutilla | Sambayon
data Helado = Vasito Gusto | Pote Gusto Gusto Gusto


compose :: (a -> b) -> (c -> a) -> c -> b
compose f1 f2 e = f1(f2 e)

uncurry :: (a -> b -> c) -> (a , b) -> c
uncurry = (\f -> (\p -> f (fst p) (snd p)))