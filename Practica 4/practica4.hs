## Practica 4

### Ex 3
Dada la siguiente definición para la función *twice*,

twice = \f -> \x -> f (f x)
a. tiene 2 redex, twice y twice doble
b. 3 redex, twice, tiwice doble, twice doble 2
c. 1 redex, twice

### Ex 4
Dada la siguiente definición para la función *twice*,

twice f = g 
    where g x = f (f x) 

determinar cuántos y cuáles son los redexes en las siguientes expresiones.

a. 1 redex, twice doble
b. 2 redex, twice doble, twice doble 2
c. 0 redex, solo hay twice y necesito un argunmento (parametro o funcion) para poder reducirse, ya que eso nos dice la definicion de twice en este punto


### Ex 5
Dada la siguiente definición para la función *twice*,

twice f x = f (f x)

determinar cuántos y cuáles son los redexes en las siguientes expresiones.

a. 0 redex, necesita un parametro
b. 1 redex, twice doble 2
c. 0 redex, necesita parametro