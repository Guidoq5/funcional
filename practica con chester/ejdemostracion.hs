a. para todo xs. para todo ys.length (xs ++ ys) = length xs + length ys

Prop: ¿para todo xs, todo ys. length (xs ++ ys) = length xs + length ys?

Dem.: sean xs,ys listas, por induccion sobre la estructura de xs
    
    Caso base, xs=[], ys=ys)  ¿ length ([]++ys) = length [] + length ys?

    Caso inductivo, xs=(x:xs'), ys=ys)
     
        HI) ¡ length (xs'++ys) = length xs' + length ys!
        TI)¿length ((x:xs')++ys) = length (x:xs') + length ys ?

Caso base, xs=[], ys=ys)  ¿ length ([]++ys) = length [] + length ys ?

length [] + length ys
                    -- = por def de length.1 con xs=[]
0 + length ys
                    -- = por aritmetica
length ys
                    -- = por def de (++) con xs=[], ys=ys
length ([]++ys)

        VALE PARA ESTE CASO

Caso inductivo, xs=(x:xs'), ys=ys)
     
        HI) ¡length xs' + length ys = length (xs'++ys)!
        TI)¿length (x:xs') + length ys = length ((x:xs')++ys)?

Lado Izq:
length (x:xs') + length ys
                        -- = por def de length.2 con xs= (x:xs')
1 + length xs' + length ys
                        -- = HI
1 + length (xs'++ys)
------------------------------------------------------------
Lado Der:
length ((x:xs')++ys)
                    -- = por def de (++) con xs= (x:xs'), ys=ys
length (x:xs'++ys)
                    -- = por def de length.2 con xs=(x:xs'++ys)
1 + length (xs'++ys)

==== VALE ASI PARA TODOS LOS CASOS ======