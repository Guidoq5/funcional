-- Ejercicio 4) Dada la siguiente definición
data AppList a = Single a 
	| Append (AppList a) (AppList a)

funcion (Single x) 		 = ...
funcion (Append al1 al2) = ... (funcion al1) ... (funcion al2)

-- i. lenAL :: AppList a -> Int , que describe la cantidad de
-- elementos de la lista.

lenAL :: AppList a -> Int
lenAL (Single x) 	   = 1
lenAL (Append al1 al2) = (lenAL al1) + (lenAL al2)


-- ii. consAL :: a -> AppList a -> AppList a , que describe la
-- lista resultante de agregar el elemento dado al principio de la lista
-- dada.
consAL :: a -> AppList a -> AppList a
consAL e al =  Append (Single e) al

-- iii. headAL :: AppList a -> a , que describe el primer elemento de
-- la lista dada.
headAL :: AppList a -> a
headAL (Single x) 		 = x
headAL (Append al1 _)  = headAL al1

-- iv. tailAL :: AppList a -> AppList a , que describe la lista
-- resultante de quitar el primer elemento de la lista dada.

-- Precondición: no puede ser una appList de un elemento.
tailAL :: AppList a -> AppList a
tailAL (Append al1 al2) = 
						case al1 of
						Single _  -> al2
						Append _ _-> Append (tailAL al1) al2


tailAL' (Append (Single _) al2) = al2
tailAL' (Append al1 al2)		= Append (tailAL al1) al2

-- v. snocAL :: a -> AppList a -> AppList a , que describe la
-- lista resultante de agregar el elemento dado al final de la lista dada.
snocAL :: a -> AppList a -> AppList a
snocAL x al = Append al (Single x)

-- vi. lastAL :: AppList a -> a , que describe el último elemento de
-- la lista dada.
lastAL :: AppList a -> a
lastAL (Single x) 		 = x
lastAL (Append _ al2) = lastAL al2

-- vii. initAL :: AppList a -> AppList a , que describe la lista dada
-- sin su último elemento.
initAL :: AppList a -> AppList a
initAL (Append al1 al2) = case al2 of
							Single _ -> al1
							Append _ _ -> Append al1 (initAL al2)

-- viii. reverseAL :: AppList a -> AppList a , que describe la lista
-- dada con sus elementos en orden inverso.
reverseAL :: AppList a -> AppList a
reverseAL (Single x) 		= Single x
reverseAL (Append al1 al2)  = Append (reverseAL al2) (reverseAL al1)  

-- Append (Single 1) (Single 2) => Append (Single 2) (Single 1) 

-- ix. elemAL :: Eq a => a -> AppList a -> Bool , que indica si el
-- elemento dado se encuentra en la lista dada.
elemAL :: Eq a => a -> AppList a -> Bool
elemAL e (Single x) 	  = e==x
elemAL e (Append al1 al2) = (elemAL e al1) || (elemAL e al2)

-- x. appendAL :: AppList a -> AppList a -> AppList a , que
-- describe el resultado de agregar los elementos de la primera lista
-- adelante de los elementos de la segunda.
-- NOTA: buscar la manera más eficiente de hacerlo.
appendAL :: AppList a -> AppList a -> AppList a
appendAL al1  al2 = Append al1 al2

-- xi. appListToList :: AppList a-> [a] , que describe la
-- representación lineal de la lista dada.
appListToList :: AppList a-> [a]
appListToList (Single x) 	   = [x]
appListToList (Append al1 al2) =(appListToList al1) ++ (appListToList al2)

-- i. para todo xs :: AppList a . para todo ys :: AppList a .
-- lenAL (appendAL xs ys ) = lenAL xs + lenAL ys

Prop: ¿para todo xs, todo ys.  lenAL (appendAL xs ys ) = f (lenAL xs + lenAL ys)?

Dem.: sean xs::[a], ys::[a]

lenAL (appendAL xs ys)
			-- = por def de appendAL
lenAL (Append xs ys)
			-- por def de lenAl.2
lenAL al1 + lenAL al2

DEMOSTRADO POR EXTENSIONALIDAD.


-- ii. reverseAL . reverseAL = id
reverseAL . reverseAL = id
					-- = por ppio de extensionalidad				
(reverseAL . reverseAL) al = id al


(reverseAL . reverseAL) al
				-- = por def de (.)
reverseAL (reverseAL al)


id al 
	-- = por def de id
al

¿(reverseAL . reverseAL) al = id al?
						-- = por demostracion de (.) y id, es lo mismo que demostrar:
¿reverseAL (reverseAL al) = al?

Prop.: ¿para todo al . reverseAL (reverseAL al) = al?

Dem.: Sea al::AppList a, por ppio de induccion sobre la estructura de al:

Caso base, al=Single x, donde  x es del tipo del elemento de al ) 
								¿reverseAL (reverseAL (Single x)) = (Single x)?

Caso Inductivo, al=Append al1 al2 ) 
	
	HI.1) ¡reverseAL (reverseAL al1) = al1!
	HI.2) ¡reverseAL (reverseAL al2) = al2!
	TI) ¿reverseAL (reverseAL (Append al1 al2)) = (Append al1 al2)?

Caso base ) ¿reverseAL (reverseAL (Single x)) = (Single x)?

reverseAL (reverseAL (Single x))
						-- = por def de reverseAL.1
reverseAL (Sigle x)
						-- = por def de reverseAL.1
Sigle x

	VALE PARA ESTE CASO.

Caso Inductivo, al=Append al1 al2 ) ¿reverseAL (reverseAL (Append al1 al2)) = Append al1 al2?

reverseAL (reverseAL (Append al1 al2))
					-- = por def de reverseAL.2
reverseAL (Append (reverseAL al2) (reverseAL al1))
					-- = por def de reverseAL.2
Append (reverseAL (reverseAL al1)) (reverseAL(reverseAL al2)) 					
					-- = por def de HI.1
Append al1 (reverseAL (reverseAL al2))
					-- = por def de H.2
Append al1 al2
	
	==== VALE LA PROPIEDAD ====

-- iii. reverseAL . consAL e . reverseAL = snocAL e


Prop.: ¿reverseAL . consAL e . reverseAL = snocAL e?

¿reverseAL . consAL e . reverseAL = snocAL e?
					-- = por ppio de extensionalidad es lo mismo que demostrar :
¿(reverseAL . consAL e . reverseAL) al= snocAL e al?
					-- = por (.) es lo mismo que demostrar:
¿reverseAL ( consAL e  (reverseAL al)) = snocAL e al?

reverseAL ( consAL e  (reverseAL al)) = snocAL e al?

Lado Izquierdo:

reverseAL ( consAL e  (reverseAL al))
							-- = por def de consAL
reverseAL (Append (Single e) (reverseAL al))
							-- por def de reverseAL.2
Append (reverseAL (reverseAL al)) (reverseAL (Single e))
							-- por def de reverseAL.1
Append (reverseAL (reverseAL al)) (Single e)
							-- = por Lema del punto anterior
Append al (Single e)


Lado Derecho:
snocAL e al
		-- = por def de snocAL
Append al (Single e)

	VALE LA PROPIEDAD		
