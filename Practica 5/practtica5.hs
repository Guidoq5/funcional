## Practica 5

### Ex 1
data Gusto = Chocolate | DulceDeLeche | Frutilla | Sambayon
data Helado = Vasito Gusto
| Cucurucho Gusto Gusto
| Pote Gusto Gusto Gusto
chocoHelate consH = consH Chocolate

chocoHelate Cucurucho Frutilla
chocoHelate Pote Frutilla SambayonW

a. (Vasito) :: Gusto -> Helado
b. Chocolate - tipo Gusto
c. Cucurucho :: Gusto -> Gusto -> Helado
d. Sambayón :: Gusto
e. (Pote) :: Gusto -> Gusto -> Gusto -> Helado
f. chocoHelate :: (Gusto -> a) -> a
g. chocoHelate Vasito :: Helado
h. chocoHelate Cucurucho :: Gusto -> Helado
i. chocoHelate (Cucurucho Sambayon) :: Helado
j. chocoHelate (chocoHelate Cucurucho) :: Helado
k. chocoHelate (Vasito DulceDeLeche) :: Error "error de tipo"
l. chocoHelate Pote :: Gusto -> Gusto -> Helado
m. chocoHelate (chocoHelate (Pote Frutilla)) :: H

###EX 4

data Medida = Mm   Float | Cm   Float  | Inch Float | Foot Float 

asMm :: Medida -> Medida
asMm (Mm f) = f
asMm (Cm f) = (Mm (f * 10))
asMm (Inch f) = //hay que convertir a mm
asMm (Foot f) = //hay que convertir a mm 


### Ex 5

Determinar el tipo de las siguientes expresiones:

uncurry Rect :: (Float , Float) -> Shape
construyeShNormal (flip Rect 5.0) :: Shape
compose (uncurry Rect) swap :: (Float , Float) -> Shape
uncurry Cucurucho :: (Gusto, Gusto) -> Helado
uncurry Rect swap :: (Float, Float) -> Shape
compose uncurry Pote :: Gusto -> (Gusto, Gusto) -> Helado
compose Just :: (b -> a) -> b -> Maybe a
compose uncurry (Pote Chocolate) :: no tiene tipo

data Shape = Circle Float | Rect Float Float 

construyeShNormal :: (Float -> Shape) -> Shape 

construyeShNormal c = c 1.0

curry :: ((a , b) -> c) -> a -> b -> c
uncurry :: (a -> b -> c) ->  ((a , b) -> c)

---
f :: a -> b
x :: a

======
f x :: b

uncurry :: (a -> b -> c) -> (a , b) -> c
Pote Chocolate :: Gusto -> Gusto -> Helado
compose :: (a -> b) -> (c -> a) -> c -> b


uncurry (pote Chocolate) :: (Gusto, Gusto) -> Helado

compose uncurry (Pote Chocolate) :: 

uncurry . Gusto -> Gusto -> Helado :: 

:t uncurry compose (Pote Chocolate) <---- !!
                                       
----
Just :: a -> Maybe a
data Maybe a = Nothing | Just a

compose :: (a -> b) -> (c -> a) -> c -> b
compose f1 f2 e = f1(f2 e)

compose Just :: (b -> a) -> b -> Maybe a


---
--compose toma una funcion, toma otra funcion, un elemento y devuelve el resultado de aplicar la 2da funcion al elemento y aplicar la 1ra funcion a ese resultado
compose :: (a -> b) -> (c -> a) -> c -> b
uncurry :: (a -> b -> c) -> (a , b) -> c
pote :: Gusto -> Gusto -> Gusto -> Helado 

uncurry Pote :: (Gusto, Gusto) -> Gusto -> Helado

compose uncurry Pote :: Gusto -> (Gusto,Gusto) -> Helado
uncurry . pote :: Gusto -> (Gusto, Gusto) -> Helado

-----
uncurry :: (a -> b -> c) -> (a , b) -> c
Rect :: Float -> Float -> Shape
swap :: (a,b) -> (b,a)

uncurry Rect :: (Float , Float) -> Shape


uncurry Rect swap :: (Float, Float) -> Shape

-----

uncurry :: (a -> b -> c) -> (a , b) -> c
uncurry = (\f -> (\p -> f (fst p) (snd p)))

uncurry cucurucho :: (Gusto, Gusto) -> Helado

----

compose (uncurry Rect) swap :: (a,b) -> Shape

compose :: (a -> b) -> (c -> a) -> c -> b
uncurry Rect :: (Float , Float) -> Shape
compose (uncurry Rect) :: (c -> (Float , Float)) -> c -> Shape
swap :: (a,b) -> (b,a)
compose (uncurry Rect) swap :: (Float , Float) -> Shape


construyeShNormal :: (Float -> Shape) -> Shape
flip Rect 5.0 :: Float -> Shape
construyeShNormal (flip Rect 5.0) :: Shape

flip :: (a -> b -> c) -> (b -> a -> c)
Rect :: Float -> Float -> Shape
flip Rect :: Float -> Float -> Shape
5.0 :: Float
flip Rect 5.0 :: Float -> Shape


uncurry :: (a -> b -> c) -> (a , b) -> c
Rect :: Float -> Float -> Shape
uncurry Rect :: (Float , Float) -> Shape

---
