data Pizza = Prepizza
           | Capa Ingrediente Pizza

data Ingrediente = Jamon | Queso

cantCapQCump :: (Ingrediente -> Bool) -> Pizza -> Int
cantCapQCump p Prepizza = 0
cantCapQCump p (Capa i ps) =
    bool2Int (p i) + (cantCapQCump p ps)

cantCapQCump' :: (Ingrediente -> Bool) -> Pizza -> Int
cantCapQCump' p = 
    pizzaProcesada
       (\i r -> bool2Int (p i) + r) 0

bool2Int True  = 1
bool2Int False = 0

esQueso Queso = True
esQueso _     = False

aptaIntolerantesLactosa p = cantCapQCump esQueso p == 0
-- aptaIntolerantesLactosa = (== 0) . cantCapQCump esQueso

pizzaProcesada :: (Ingrediente -> b -> b) 
               -> b 
               -> Pizza
               -> b
pizzaProcesada fi zpp Prepizza    = zpp
pizzaProcesada fi zpp (Capa i ps) =
    fi i (pizzaProcesada fi zpp ps)

conCapTransf :: (Ingrediente -> Ingrediente)
             -> Pizza
             -> Pizza
conCapTransf f =
    pizzaProcesada (Capa . f) Prepizza


capasQueCumplen :: (Ingrediente -> Bool) 
                -> Pizza
                -> [Ingrediente]
capasQueCumplen p = 
    pizzaProcesada (agregarSi p) []

agregarSi p i =
    if p i
       then (i:)
       else id

conCapasDe_1 :: Pizza -> Pizza -> Pizza
conCapasDe_1 =
    pizzaProcesada
       (\i r -> Capa i . r) id

-- pizzaProcesada :: (Ingrediente -> (Pizza -> Pizza) -> (Pizza -> Pizza)) 
--                -> (Pizza -> Pizza)
--                -> Pizza
--                -> (Pizza -> Pizza)

conCapasDe_2 :: Pizza -> (Pizza -> Pizza)
conCapasDe_2 =
    pizzaProcesada g fz
    where g i fr = Capa i . fr
          fz     = id

conCapasDe_3 :: Pizza -> (Pizza -> Pizza)
conCapasDe_3 =
    pizzaProcesada g fz
    where g i fr = (\rpz2 -> Capa i (fr rpz2))
          fz     = (\pz2 -> pz2)

conCapasDe_4 :: Pizza -> (Pizza -> Pizza)
conCapasDe_4 =
    pizzaProcesada g fz
    where g i fr rpz2 = Capa i (fr rpz2)
          fz     pz2  = pz2

conCapasDe_5 :: Pizza -> (Pizza -> Pizza)
conCapasDe_5 =
    pizzaProcesada
          (\i fr rpz2 -> Capa i (fr rpz2))
          (\pz2 -> pz2)

conCapasDe_6 :: Pizza -> (Pizza -> Pizza)
conCapasDe_6 =
    pizzaProcesada
          (\i fr -> Capa i . fr)
          id

conCapasDe_7 :: Pizza -> (Pizza -> Pizza)
conCapasDe_7 =
    pizzaProcesada
          (\i -> (Capa i .))
          id

cantCapas = pizzaProcesada (\i -> (1 +)) 0


append_1 :: [a] -> ([a] -> [a])
append_1 []     = (\ys -> ys)
append_1 (x:xs) = (\ys -> x : append_1 xs ys)

append_2 :: [a] -> ([a] -> [a])
append_2 []     = id
append_2 (x:xs) = (x:) . append_2 xs
             -- (\ys -> x : (append_2 xs ys))
             --         f    g

append_1' =
    foldr (\x r ys -> x : r ys)
          (\ys -> ys)

append_2' =
    foldr (\x r -> (x:) . r) id

    -- foldr (\x -> (.) (x:)) id

    -- foldr (\x -> (.) ((:) x)) id
    -- --           f    g

    -- foldr ((.) . (:)) id

-- foldr :: (a -> b -> b) -> b -> [a] -> b

-- sum [] = 0
-- --       z
-- sum (x:xs) = x + sum xs
-- --           x + r

sum_1 = foldr (\x r -> x + r) 0

sum_2 = foldr (+) 0

prod_1 = foldr (*) 1

length_1 = foldr (\x r -> 1 + r) 0
      -- foldr (const (+1)) 0
               
      --          const (+1)
      -- foldr :: (a -> (Int -> Int)) -> Int -> [a] -> Int

-- demo inventa recién, fresca
-- foldr (const (+1)) 0 = foldr (+) 0 . fold (const (1 :)) []

zip_1 :: [a] -> ([a] -> [(a, a)])
zip_1 []     ys     = []
zip_1 (x:xs) []     = []
zip_1 (x:xs) (y:ys) = (x,y) : zip_1 xs ys

zip_2 = foldr g fz
  where
       g x r []     = []
       g x r (y:ys) = (x,y) : r ys
       fz    ys     = []

-- zip_3 = foldr
--    (\x r ys ->
--       case ys of 
--          []     -> []
--          (y:ys) -> (x,y) : r ys)
--    (\ys -> [])


take_1 []     n = []
take_1 (x:xs) 0 = []
take_1 (x:xs) n = x : take_1 xs (n-1)

take_2 = foldr g fz
   where g x r 0 = []
         g x r n = x : r (n-1)
         fz    n = []

head []     = error "no tiene elementos"
head (x:xs) = x

--      foldr :: (a -> a -> a) -> a -> [a] -> a
head' = foldr (\x r -> x) (error "no tiene elementos")
--            const

-- tomar el nro primero
drop_1 []     n = []
drop_1 (x:xs) 0 = x : xs
drop_1 (x:xs) n = drop_1 xs (n-1)

recr :: (a -> [a] -> b -> b) -> b -> [a] -> b
recr f z []     = z
recr f z (x:xs) = f x xs (recr f z xs)

drop_2 = recr f z
   where f x xs r 0 = x : xs
         f x xs r n = r (n-1)
         z        n = []
