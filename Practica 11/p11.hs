foldr' :: (a->b->b) -> b -> [a] -> b
foldr' f z [] = z
foldr' f z (x:xs) = f x (foldr' f z xs)

data Pizza = Prepizza | Capa Ingrediente Pizza deriving Show
data Ingrediente = Aceitunas Int | Jamon | Queso| Salsa deriving Show

pizza1 = Capa Salsa (Capa Jamon (Capa Jamon (Capa Queso (Capa (Aceitunas 5) (Capa Queso (Capa Salsa (Capa Jamon Prepizza)))))))

pizza2 = (Capa (Aceitunas 3)
			(Capa (Aceitunas 5)
				(Capa Queso 
					(Capa Salsa Prepizza))))

cantidadCapasQueCumplen :: (Ingrediente -> Bool) -> Pizza -> Int 
cantidadCapasQueCumplen f Prepizza = 0
cantidadCapasQueCumplen f (Capa i ps) = boolToInt (f i) + cantidadCapasQueCumplen f ps

cantidadCapasQueCumplen' :: (Ingrediente -> Bool) -> Pizza -> Int
cantidadCapasQueCumplen' p = pizzaProcesada f 0 
							where f i r = boolToInt(p i) + r -- la r seria la pizza? es decir la recursion? o de donde sale?

boolToInt :: Bool -> Int
boolToInt True = 1
boolToInt False = 0

----------------------------------------------------------------------------------

conCapasTransformadas  :: (Ingrediente -> Ingrediente) -> Pizza -> Pizza
conCapasTransformadas f Prepizza = Prepizza
conCapasTransformadas f (Capa i ps) = Capa (f i) (conCapasTransformadas f ps)

conCapasTransformadas'  :: (Ingrediente -> Ingrediente) -> Pizza -> Pizza
conCapasTransformadas' g = pizzaProcesada f Prepizza
						  where f i r = Capa (g i) r

----------------------------------------------------------------------------------

soloLasCapasQue :: (Ingrediente -> Bool) -> Pizza -> Pizza
soloLasCapasQue f Prepizza = Prepizza
soloLasCapasQue f (Capa i p) = if(f i)
								then (Capa i (soloLasCapasQue f p))
								else soloLasCapasQue f p

soloLasCapasQue'  :: (Ingrediente -> Bool) -> Pizza -> Pizza
soloLasCapasQue' p = pizzaProcesada f Prepizza
					 where f i r = if (p i) then (Capa i r) else r

----------------------------------------------------------------------------------

primerasNCapas :: Int -> Pizza -> Pizza
primerasNCapas 0 p = Prepizza
primerasNCapas n Prepizza = Prepizza
primerasNCapas n (Capa i p) = (Capa i (primerasNCapas (n-1) p))

primerasNCapas' :: Int -> Pizza -> Pizza
primerasNCapas' n p = pizzaProcesada f casoBase p n -- donde se usa la n ? porque en vez de casoBase no ponemos Prepizza?, la P ahora porque "si" la ponemos ?
                     where f i r 0 = Prepizza
                           f i r n'= (Capa i (r (n' - 1)))
                           casoBase n' = Prepizza

pizzaProcesada :: (Ingrediente -> b -> b) -> b -> Pizza -> b
pizzaProcesada f cb Prepizza = cb
pizzaProcesada f cb (Capa i ps) = f i (pizzaProcesada f cb ps)

----------------------------------------------------------------------------------

cantAceitunas :: Pizza -> Int
cantAceitunas Prepizza = 0
cantAceitunas (Capa i ps) = aceitunas i + cantAceitunas ps

aceitunas (Aceitunas n) =  n
aceitunas _             = 0

-- cantAceitunas' :: Pizza -> Int
-- cantAceitunas' p = pizzaProcesada f 0
-- 					where f i r = (aceitunas i) + r

----------------------------------------------------------------------------------


-- Ejercicio 7)


-- cantidadCapasQueCumplen' :: (Ingrediente -> Bool) -> Pizza -> Int
-- cantidadCapasQueCumplen' p = pizzaProcesada f 0 
-- 							where f i r = boolToInt(p i) + r -- la r seria la pizza? es decir la recursion? o de donde sale?


-- A)
map' :: (a -> b) -> [a] -> [b]
map' f [] = []
map' f (x:xs) = (f x) : map f xs

mapF :: (a -> b) -> [a] -> [b]
mapF f = foldr g []
		  where g n r = (f n): r

filter' :: (a -> Bool) -> [a] -> [a]
filter' f [] = []
filter' f (x:xs) = if (f x) then x : filter' f xs else filter' f xs

filterF :: (a -> Bool) -> [a] -> [a]
filterF p = foldr' g []
			where g i r = if(p i) then i:r else r

sumF :: [Int] -> Int
sumF = foldr (+) 0

lengthF :: [a] -> Int
lengthF xs = foldr' g 0 xs -- Entonces la r seria xs no ? y se podria sacar xs de ambos lados
			 where g i r = 1 + r

mylength :: [a] -> Int
mylength xs = foldr f 0 xs
    where f x r = 1 + r

find :: (a -> Bool) -> [a] -> Maybe a
find p = foldr g Nothing
		 where g i r = if (p i) then Just i else r

