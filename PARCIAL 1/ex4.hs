Tengo que cambiar la definicion de appendST ya que no tengo los casos "Zero" y "One":

appendST :: SumTree -> SumTree -> SumTree
appendST Zero st = st 
appendST One st = SumT One st
appendST (SumT st1 st2) st3 = SumT st3 (appendST st1 st2)



para todo ns. para todo ms. 

	appendST (nl2st ns) (nl2st ms) = nl2st (appendL ns ms)


Prop: ¿para todo ns, todo ms. appendST (nl2st ns) (nl2st ms) = nl2st (appendL ns ms) ?


Dem.: sean ns:: List a, por induccion sobre la estructura de ns

Caso base, ns=Nil, ms=ms)  ¿  appendST (nl2st Nil) (nl2st ms) = nl2st (appendL Nil ms)?

Caso inductivo, ns= Cons a (List a), ms=ms)
 
    HI) ¡  appendST (nl2st (List a)) (nl2st ms) = nl2st (appendL (List a) ms) !
    TI) ¿ appendST (nl2st( Cons a (List a)) ) (nl2st ms) = nl2st (appendL (Cons a (List a)) ms) ?


Caso base, ns=Nil, ms=ms)  ¿  appendST (nl2st Nil) (nl2st ms) = nl2st (appendL Nil ms)?


lado Izq)

appendST (nl2st Nil) (nl2st ms)
--						por def nl2st.1
appendST Zero (nl2st ms)
--						por def de appendST.1
nl2st ms


lado Der)

nl2st (appendL Nil ms)
--				por def appendL.1
nl2st ms

VALE PARA ESTE CASO


Caso inductivo, ns= Cons a (List a), ms=ms)
 
    HI) ¡  appendST (nl2st (List a)) (nl2st ms) = nl2st (appendL (List a) ms) !
    TI) ¿ appendST (nl2st( Cons a (List a)) ) (nl2st ms) = nl2st (appendL (Cons a (List a)) ms) ?

¿ appendST (nl2st( Cons a (List a)) ) (nl2st ms) = nl2st (appendL (Cons a (List a)) ms) ?


lado Izq) 
appendST (nl2st( Cons a (List a)) ) (nl2st ms)
-- 											por def de n12st.2 
appendST (SumT One (nl2st (List a))) (n12st ms)
-- 											por def de appendST.3
SumT (n12st ms) (AppendST One (nl2st (List a)))


lado Der)
nl2st (appendL (Cons a (List a)) ms)
--				por def appendL.3
nl2st (Cons a (appendL (List a) ms))
--				por def nl2st.2
SumT One (n12st (appenDL (List a) ms ))