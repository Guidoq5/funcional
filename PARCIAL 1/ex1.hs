data List a = Nil | Cons a (List a)
data SumTree = Zero
 | One
 | SumT SumTree SumTree

appendL :: List a -> List a -> List a
appendL Nil ys = ys
appendL (Cons x xs) ys = Cons x (appendL xs ys)

st2nl Zero = Nil
st2nl One = Cons () Nil
st2nl (SumT s1 s2) = appendL (st2nl s1) (st2nl s2)



-- st2n1 :: SumTree -> List ()

-- st2n1 :: SumTree -> List a

nl2st Nil = Zero
nl2st (Cons e es) = SumT One (nl2st es)

-- nl2st :: List a -> SumTree


flip' f x y = f y x
subst f g x = f x (g x)

fsubst = flip' subst


Defino el tipo de flip
flip::(a -> b -> c) -> b -> a -> c
flip' f x y = f y x

Defino el tipo de subst
subst :: (a -> b -> c) ->(a-> b) -> a-> c
subst f g x = f x (g x)

Tipo de fsubst
fsubst:: (a -> b) -> (a -> b -> c) -> a -> c