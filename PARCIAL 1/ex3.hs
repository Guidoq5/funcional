data List a = Nil | Cons a (List a) deriving Show
data SumTree = Zero
                  | One
                        | SumT SumTree SumTree deriving Show

ejemplo = SumT (SumT Zero One) (SumT Zero One)


appendL :: List a -> List a -> List a
appendL Nil ys = ys
appendL (Cons x xs) ys = Cons x (appendL xs ys)

st2nl Zero = Nil
st2nl One = Cons () Nil
st2nl (SumT s1 s2) = appendL (st2nl s1) (st2nl s2)

nl2st Nil = Zero
nl2st (Cons e es) = SumT One (nl2st es)

normST :: SumTree -> SumTree
normST Zero = Zero
normST One = SumT One Zero
normST (SumT s1 s2) = appendST s1 s2

appendST :: SumTree -> SumTree -> SumTree
appendST (SumT st1 st2) st3 = SumT st3 (appendST st1 st2)
