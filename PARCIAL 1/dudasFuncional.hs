--Dudas Practica 2:

--Como llegue a definir los tipos a estas funciones ?

const :: a -> (b -> b)
const x = g 
    where g y = y -- OK

appFork :: ((a -> b), (a -> c)) -> a -> (b, c)
appFork (f, g) = h 
    where h x = (f x, g x) -- OK

appDist :: (a -> b) ->  (a,a) -> (b,b) 
appDist f = g 
    where g (x,y) = (f x, f y) --OK

uflip :: ((a, b) -> c) -> ((b, a) -> c)
uflip f = g
    where g p = f (swap p) --OK

--Dudas Practica 3:

--Reescribir las siguientes definiciones sin utilizar​where​,​leto lambdas, y  utilizando la menor cantidad de paréntesis posible.

a. flip f = g   
    where g x = h   
        h y = (f y) x -- OK
        

b. uflip f = g   
    where g p = f (swap p) -- OK



--Reescribir las siguientes definiciones utilizando sólo lambdas (sin ​where ni ​let​).

c. appDup f = g   
    where g x = f (x, x) -- OK


d. subst f = h   
    where h g = k     
        where k x = (f x) (g x)


--Dudas Practica 6:

Demostrar las siguientes propiedades: 

a. curry suma' = suma 

b. uncurry suma = suma'

c. curry (uncurry f) = f 

d. uncurry (curry f') = f

Dada la siguiente definición (f . g) x = f (g x) 

a. definir las siguientes funciones utilizando el operador ​(.) y la menor
cantidad de parámetros posible: 

i. cuadruple 

ii. doble 

iii. twice 

iv. many 
