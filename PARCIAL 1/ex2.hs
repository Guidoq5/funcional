Dadas las siguientes definiciones

      apply f x = f x
      compose f g x = f (g x)
      const x y = x
      flip f x y = f y x
      subst f g x = f x (g x)
      curry f x y = f (x, y)
      fsubst = flip subst
      csubst = compose subst const

Demostrar que

      para todo f. para todo g.  fsubst g (const f)  =  compose (apply f) g


Prop: ¿ fsubst g (const f)  =  compose (apply f) g ?

¿ fsubst g (const f)  =  compose (apply f) g ?
-- 										por ppio de extensionalidad
¿ fsubst g (const f) x =  compose (apply f x) g ?
-- 										por def de compose
¿ fsubst g (const f) x =  apply f (g x) ?


Lado izq)

fsubst g (const f) x
--			por def de fsubst
flip subst g (const f) x
--			por def de flip
subst (const f) g x
--                por def de subst
(const f x) (g x)
--                por def de const
f (g x)


Lado der)

apply f (g x)
		-- por def apply
f (g x)

VALE LA PROPIEDAD